import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { PrivacyService } from './../../api/api-services/master/privacy.service';
import { LevelsService } from './../../api/api-services/master/levels.service';
import { GradesService } from './../../api/api-services/master/grades.service';
import { SubjectsService } from './../../api/api-services/master/subjects.service';
import { ReportsService } from './../../api/api-services/reports/reports.service';

@Component({
  selector: 'app-note-overview',
  templateUrl: './note-overview.component.html',
  styleUrls: ['./note-overview.component.css']
})
export class NoteOverviewComponent implements OnInit, OnDestroy {

  subjectOption;
  gradeOption;
  levelOption;
  privacyOption;
  filterForm;
  processing = false;

  // Graph options
  view: any[] = [1000, 400];
  multi = [];
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = '';
  showYAxisLabel = true;
  yAxisLabel = '#Notes';
  colorScheme = {
    domain: ['#5AA454', '#A10A28']
  };
  autoScale = true;
  private alive = true;
  private queryBy = 'day';
  constructor(
    private subjectsService: SubjectsService,
    private gradesService: GradesService,
    private levelsService: LevelsService,
    private privacyService: PrivacyService,
    private reportsService: ReportsService
  ) {
    this.subjectOption = this.subjectsService.get();
    this.gradeOption = this.gradesService.get();
    this.levelOption = this.levelsService.get();
    this.privacyOption = this.privacyService.get();
  }

  ngOnInit() {
    this.initialFormData();
  }

  onQueryByClick(queryBy) {
    this.queryBy = queryBy;
    this.getNotesOverview();
  }

  private initialFormData() {
    try {
      this.filterForm = new FormBuilder().group({
        subject: [null],
        grade: [null],
        difficulty: [null],
        privacy: ['paid']
      });
      this.filterForm.valueChanges.subscribe(data => { this.getNotesOverview(); });
      this.getNotesOverview();
    } catch (exception) {
      console.log(exception);
    }
  }

  private getNotesOverview() {
    this.processing = true;
    this.reportsService.getNotesOverview(this.filterForm.get('subject').value, this.filterForm.get('grade').value,
      this.filterForm.get('difficulty').value, this.filterForm.get('privacy').value, this.queryBy)
      .takeWhile(alive => this.alive)
      .subscribe(res => {
        this.multi = res;
        this.processing = false;
      }, error => {
        console.log(error);
        this.processing = false;
      });
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
