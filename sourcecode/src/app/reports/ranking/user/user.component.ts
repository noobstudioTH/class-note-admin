import { Component, OnInit, OnDestroy, ViewChild, TemplateRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {

  @ViewChild('editTmpl') editTmpl: TemplateRef<any>;
  @ViewChild('hdrTpl') hdrTpl: TemplateRef<any>;
  sortingForm;
  dataList;
  columnList = [{ prop: 'rank', name: 'Rank' }, { prop: 'name', name: 'Name' },
  { prop: 'rating', name: 'Rating' }, { prop: 'follower', name: 'Follower' },
  { prop: 'following', name: 'Following' }, { prop: 'note', name: '#Notes' },
  { prop: 'serie', name: '#Series' }, { prop: 'transaction', name: '#Transactions' },
  { name: 'Action', cellTemplate: this.editTmpl, headerTemplate: this.hdrTpl }];
  private alive = true;
  private queryBy = 'day';
  sortingOption = [
    { code: 'rating', text: 'Rating' },
    { code: 'follower', text: 'Follower' },
    { code: 'following', text: 'Following' },
    { code: 'note', text: '#Notes' },
    { code: 'series', text: '#Series' },
    { code: 'transaction', text: '#Transactions' }
  ];
  constructor() { }

  ngOnInit() {
    this.initialFormData();
  }

  onQueryByClick(queryBy) {
    this.queryBy = queryBy;
  }

  setPage($event) {
    this.dataList.start = $event.offset * $event.pageSize;
    this.dataList.end = this.dataList.start + $event.pageSize;
    this.dataList.offset = $event.offset;
  }

  private initialFormData() {
    try {
      this.sortingForm = new FormBuilder().group({
        sorting: ['rating'],
      });
      this.sortingForm.valueChanges.subscribe(data => { });
      this.dataList = {
        offset: 0,
        limit: 10,
        loadingIndicator: false,
        columns: this.columnList,
        rows: [],
        total: 0,
        start: 0,
        end: 10
      };
    } catch (exception) {
      console.log(exception);
    }
  }

  private getDataList() {

  }

  ngOnDestroy() {
    this.alive = false;
  }

}
