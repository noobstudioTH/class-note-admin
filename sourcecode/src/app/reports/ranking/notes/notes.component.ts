import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { RankingService } from './../../../api/api-services/reports/ranking.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit, OnDestroy {

  sortingForm;
  dataList;
  columnList = [{ prop: 'rank', name: 'Rank' }, { prop: 'id', name: 'Note ID' }, { prop: 'name', name: 'Note Name' },
  { prop: 'writername', name: 'Writer Name' }, { prop: 'rating', name: 'Rating' }, { prop: 'view', name: '#Viewed' },
  { prop: 'favorite', name: '#Favorite' }, { prop: 'transaction', name: '#Transactions' }];
  private alive = true;
  sortingOption = [
    { code: 'rating', text: 'Rating' },
    { code: 'view', text: '#Viewed' },
    { code: 'favorite', text: '#Favorite' },
    { code: 'transaction', text: '#Transactions' }
  ];
  constructor(
    private rankingService: RankingService
  ) { }

  ngOnInit() {
    this.initialFormData();
  }

  onQueryByClick(queryBy) {
    this.dataList.queryBy = queryBy;
    this.getDataList();
  }

  setPage($event) {
    this.dataList.start = $event.offset * $event.pageSize;
    this.dataList.end = this.dataList.start + $event.pageSize;
    this.dataList.offset = $event.offset;
    this.getDataList();
  }

  private initialFormData() {
    try {
      this.sortingForm = new FormBuilder().group({
        sorting: ['rating'],
      });
      this.sortingForm.valueChanges.subscribe(data => { });
      this.dataList = {
        offset: 0,
        limit: 10,
        loadingIndicator: false,
        columns: this.columnList,
        rows: [],
        total: 0,
        start: 0,
        end: 10,
        queryBy: 'day',
        sortingBy: this.sortingForm.get('sorting').value
      };
      this.sortingForm.get('sorting').valueChanges.subscribe(data => {
        this.dataList.sortingBy = data;
        this.getDataList();
      });
      this.getDataList();
    } catch (exception) {
      console.log(exception);
    }
  }

  private getDataList() {
    this.dataList.loadingIndicator = true;
    this.rankingService.getRankingNotes(this.dataList)
      .takeWhile(alive => this.alive)
      .subscribe(dataList => {
        try {
          this.dataList.rows = dataList.data;
          this.dataList.total = dataList.total;
        } catch (exception) {
          console.log(exception);
        } finally {
          this.dataList.loadingIndicator = false;
        }
      }, error => {
        console.log(error);
        this.dataList.loadingIndicator = false;
      });
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
