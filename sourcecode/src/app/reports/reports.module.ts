import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxChartsModule } from '@swimlane/ngx-charts';

import { NoteOverviewComponent } from './note-overview/note-overview.component';
import { ReportsRoutingModule } from './reports-routing.module';
import { SharedModule } from './../shared/shared.module';
import { NotesComponent } from './ranking/notes/notes.component';
import { UserComponent } from './ranking/user/user.component';
import { SearchComponent } from './ranking/search/search.component';

@NgModule({
  imports: [
    CommonModule,
    ReportsRoutingModule,
    SharedModule,
    NgxChartsModule
  ],
  declarations: [NoteOverviewComponent, NotesComponent, UserComponent, SearchComponent]
})
export class ReportsModule { }
