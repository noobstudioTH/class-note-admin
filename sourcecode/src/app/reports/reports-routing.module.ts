import { Routes, RouterModule } from '@angular/router';
import { NgModule, Component } from '@angular/core';

import { NoteOverviewComponent } from './note-overview/note-overview.component';
import { NotesComponent } from './ranking/notes/notes.component';
import { UserComponent } from './ranking/user/user.component';
import { SearchComponent } from './ranking/search/search.component';
import { CommingSoonComponent } from './../shared/comming-soon/comming-soon.component';

const reportsRoutes: Routes = [
    { path: 'note-overview', component: NoteOverviewComponent },
    { path: 'ranking-notes', component: NotesComponent },
    { path: 'ranking-user', component: UserComponent },
    { path: 'ranking-search', component: SearchComponent },
    { path: 'transactions', component: CommingSoonComponent },
    { path: 'real-time', component: CommingSoonComponent },
    { path: 'visitors', component: CommingSoonComponent },
    { path: 'time-spent', component: CommingSoonComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(reportsRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ReportsRoutingModule { }
