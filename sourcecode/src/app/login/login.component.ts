import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileService } from '../api/api-services/profile.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  constructor(
    private route: ActivatedRoute,
    private profileService: ProfileService,
    private router: Router
  ) {}

  private alive = true;
  ngOnInit() {
    this.login();
  }

  private login() {
    this.route.params.subscribe(params => {
      this.profileService
        .getProfileAdmin(params.uid)
        .takeWhile(alive => this.alive)
        .subscribe(profile => {
          if(profile.data["is_admin"]){
            this.profileService.profileSubject.next(profile);
            this.router.navigate(['/dashboard']);
          }
        });
    });
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
