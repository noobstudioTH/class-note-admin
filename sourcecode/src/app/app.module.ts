import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MainModule } from './main/main.module';
import { AppRoutingModule } from './app-routing.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { ApiModule } from './api/api.module';
import { ItemManagementModule } from './item-management/item-management.module';
import { UserManagementModule } from './user-management/user-management.module';
import { ReportsModule } from './reports/reports.module';
import { JwtInterceptorService } from './shared/services/interceptor/jwt-interceptor.service';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ApiModule,
    MainModule,
    AppRoutingModule,
    DashboardModule,
    ItemManagementModule,
    UserManagementModule,
    ReportsModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
