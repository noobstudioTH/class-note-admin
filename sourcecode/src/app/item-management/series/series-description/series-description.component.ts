import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-series-description',
  templateUrl: './series-description.component.html',
  styleUrls: ['./series-description.component.css']
})
export class SeriesDescriptionComponent implements OnInit {

  _descriptionForm;
  @Input()
  set descriptionForm(descriptionForm) {
    this._descriptionForm = descriptionForm;
  }
  constructor() { }

  ngOnInit() {
  }

}
