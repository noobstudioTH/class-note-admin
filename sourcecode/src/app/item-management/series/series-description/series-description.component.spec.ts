import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeriesDescriptionComponent } from './series-description.component';

describe('SeriesDescriptionComponent', () => {
  let component: SeriesDescriptionComponent;
  let fixture: ComponentFixture<SeriesDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeriesDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeriesDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
