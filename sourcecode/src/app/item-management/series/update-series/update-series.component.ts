declare const $;

import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { SeriesService } from './../../../api/api-services/item-management/series.service';

@Component({
  selector: 'app-update-series',
  templateUrl: './update-series.component.html',
  styleUrls: ['./update-series.component.css']
})
export class UpdateSeriesComponent implements OnInit, OnDestroy {

  seriesItem;
  @Input()
  set series(seriesItem) {
    if (seriesItem) {
      this.seriesItem = seriesItem.series;
      this.initialData();
    }
  }
  currentStep;
  wizardConfig;
  processing: boolean;
  processingDelete: boolean;
  descriptionForm: FormGroup;
  @Output() onAlterDataEvent = new EventEmitter<any>();
  private alive = true;
  constructor(
    private seriesService: SeriesService
  ) { }

  ngOnInit() {
  }

  onStepClick(step) {
    this.currentStep = step;
  }

  updateSeries() {
    this.processing = true;
    this.seriesService.update(this.seriesItem.id, this.descriptionForm.get('name').value
      , this.descriptionForm.get('description').value, this.extractNoteItem())
      .takeWhile(alive => this.alive)
      .subscribe(res => {
        this.processing = false;
        this.onAlterDataEvent.emit(true);
        this.toggleModal();
      }, error => {
        this.processing = false;
        this.toggleModal();
      });
  }

  onDeleteSeries() {
    this.processingDelete = true;
    this.seriesService.delete(this.seriesItem.id)
      .takeWhile(alive => this.alive)
      .subscribe(response => {
        this.processingDelete = false;
        try {
          this.onAlterDataEvent.emit(true);
          this.toggleModal();
        } catch (exception) { }
      }, error => {
        this.processingDelete = false;
      });
  }

  private toggleModal() {
    try {
      $('#update-series-modal').modal('toggle');
    } catch (exception) {
      console.log(exception);
    }
  }

  private initialData() {
    this.currentStep = 1;
    this.wizardConfig = [
      { step: 1, name: 'Series' },
      { step: 2, name: 'Add Note' }
    ];
    this.processing = false;
    this.createDescriptionForm();
    this.toggleModal();
  }

  private createDescriptionForm() {
    this.descriptionForm = new FormBuilder().group({
      name: [this.seriesItem.name, Validators.required],
      description: [this.seriesItem.description, Validators.required],
      noteItems: [this.seriesItem.notes, Validators.required]
    });
  }

  private extractNoteItem() {
    const notes = [];
    try {
      this.descriptionForm.get('noteItems').value.forEach(note => {
        if (note.id) { notes.push(note.id); } else { notes.push(note); }
      });
    } catch (exception) {
      console.log(exception);
    } finally {
      return notes;
    }
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
