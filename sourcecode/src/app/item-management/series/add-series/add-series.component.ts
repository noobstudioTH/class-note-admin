declare const $;

import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { takeWhile } from 'rxjs/operator/takeWhile';

import { SeriesService } from './../../../api/api-services/item-management/series.service';

@Component({
  selector: 'app-add-series',
  templateUrl: './add-series.component.html',
  styleUrls: ['./add-series.component.css']
})
export class AddSeriesComponent implements OnInit, OnDestroy {

  seriesItem;
  @Input()
  set series(seriesItem) {
    if (seriesItem) {
      this.initialData();
    }
  }
  currentStep;
  wizardConfig;
  processing: boolean;
  descriptionForm: FormGroup;
  @Output() onAlterDataEvent = new EventEmitter<any>();
  private alive = true;
  constructor(
    private seriesService: SeriesService
  ) { }

  ngOnInit() {
  }

  onStepClick(step) {
    this.currentStep = step;
  }

  createSeries() {
    this.processing = true;
    this.seriesService.create(this.descriptionForm.get('name').value
      , this.descriptionForm.get('description').value, this.extractNoteItem())
      .takeWhile(alive => this.alive)
      .subscribe(res => {
        this.processing = false;
        this.onAlterDataEvent.emit(true);
        this.toggleModal();
      }, error => {
        this.processing = false;
        this.toggleModal();
      });
  }

  private toggleModal() {
    try {
      $('#add-series-modal').modal('toggle');
    } catch (exception) {
      console.log(exception);
    }
  }

  private initialData() {
    this.currentStep = 1;
    this.wizardConfig = [
      { step: 1, name: 'Series' },
      { step: 2, name: 'Add Note' }
    ];
    this.processing = false;
    this.createDescriptionForm();
    this.toggleModal();
  }

  private createDescriptionForm() {
    this.descriptionForm = new FormBuilder().group({
      name: [null, Validators.required],
      description: [null, Validators.required],
      noteItems: [null, Validators.required]
    });
  }

  private extractNoteItem() {
    const notes = [];
    try {
      this.descriptionForm.get('noteItems').value.forEach(note => {
        notes.push(note.id);
      });
    } catch (exception) {
      console.log(exception);
    } finally {
      return notes;
    }
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
