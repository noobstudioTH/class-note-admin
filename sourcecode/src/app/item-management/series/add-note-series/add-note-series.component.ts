import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { environment } from './../../../../environments/environment';
import { NotesService } from './../../../api/api-services/item-management/notes.service';

@Component({
  selector: 'app-add-note-series',
  templateUrl: './add-note-series.component.html',
  styleUrls: ['./add-note-series.component.css']
})
export class AddNoteSeriesComponent implements OnInit, OnDestroy {

  leftSideList;
  rightSideList;
  optionList;
  searchText: string;
  _descriptionForm;
  @Input()
  set descriptionForm(descriptionForm) {
    this._descriptionForm = descriptionForm;
    this.getOptionList();
  }
  private alive = true;
  private optionListAll;
  constructor(
    private notesService: NotesService
  ) {
  }

  ngOnInit() {
  }

  onClick(option) {
    if (option.selected) {
      delete option.selected;
    } else {
      option.selected = true;
    }
    this.subList();
  }

  onSearch() {
    try {
      if (this.searchText) {
        this.optionList = this.optionList.filter(option => {
          if (option.caption) {
            return option.caption.includes(this.searchText);
          } else {
            return false;
          }
        });
      } else {
        this.optionList = this.optionListAll;
      }
    } catch (exception) {
      console.log(exception);
    }
  }

  private getOptionList() {
    this.notesService.getAll()
      .takeWhile(alive => this.alive)
      .subscribe(series => {
        try {
          this.optionList = series.data;
          this.optionListAll = series.data;
          const selectedNote = this._descriptionForm.get('noteItems').value;
          if (selectedNote) {
            selectedNote.forEach(note => {
              const match = series.data.find(serie => {
                return serie.id === note;
              });
              match.selected = true;
            });
            this.subList();
          }
        } catch (exception) {
          console.log(exception);
        }
      }, error => {
        console.log(error);
      });
  }

  private subList() {
    try {
      if (!this.optionList) {
        return;
      }
      const result = this.optionList.filter(option => option.selected === true);
      this._descriptionForm.get('noteItems').setValue(JSON.parse(JSON.stringify(result)));
      result.forEach(option => {
        try {
          const media = option.media;
          if (media) {
            const imagePath = media[Object.keys(media)[0]].image;
            option.image = `${environment.appResourceServer}images/${imagePath}`;
          }
        } catch (exception) {
          console.log(exception);
        }
      });
      const half = Math.ceil(result.length / 2);
      this.leftSideList = result.splice(0, half);
      this.rightSideList = result.splice(0);
    } catch (exception) {
      console.log(exception);
    }
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
