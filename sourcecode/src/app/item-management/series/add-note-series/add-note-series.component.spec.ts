import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNoteSeriesComponent } from './add-note-series.component';

describe('AddNoteSeriesComponent', () => {
  let component: AddNoteSeriesComponent;
  let fixture: ComponentFixture<AddNoteSeriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNoteSeriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNoteSeriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
