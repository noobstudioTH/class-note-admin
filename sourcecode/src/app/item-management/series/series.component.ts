import { Component, OnInit, OnDestroy } from '@angular/core';

import { Series } from './../../enum/series.enum';
import { SeriesService } from './../../api/api-services/item-management/series.service';

@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.css']
})
export class SeriesComponent implements OnInit, OnDestroy {

  dataService;
  columnList = [{ prop: 'id', name: 'Series ID' }, { prop: 'name', name: 'Series Name' },
  { prop: 'writername', name: 'Writer Name' }, { name: 'Action' }];
  formConfig = [
    [{ id: Series.seriesId, name: 'Series Id', type: 'text', placeholder: 'Series Id' },
    { id: Series.seriesName, name: 'Series Name', type: 'text', placeholder: 'Series Name' }],
    [{ id: Series.writerName, name: 'Writer Name', type: 'text', placeholder: 'Writer Name' }]
  ];
  searchForm;
  addSeries;
  updateSeries;
  onAlterData = { flag: false };
  private alive = true;

  constructor(
    private seriesService: SeriesService
  ) {
    this.dataService = seriesService;
  }

  ngOnInit() {
  }

  onsubmitEvent($event) {
    try {
      this.searchForm = { form: $event };
    } catch (exception) {
      console.log(exception);
    }
  }

  onAddSeries() {
    this.addSeries = {};
  }

  onTableRowEdit($event) {
    this.updateSeries = { series: JSON.parse(JSON.stringify($event)) };
  }

  onAlterDataEvent($event) {
    this.onAlterData = { flag: true };
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
