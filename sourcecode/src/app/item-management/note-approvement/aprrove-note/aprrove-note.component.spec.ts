import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AprroveNoteComponent } from './aprrove-note.component';

describe('AprroveNoteComponent', () => {
  let component: AprroveNoteComponent;
  let fixture: ComponentFixture<AprroveNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AprroveNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AprroveNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
