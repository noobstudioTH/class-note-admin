declare const $;

import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';

import { takeWhile } from 'rxjs/operator/takeWhile';

import { environment } from './../../../../environments/environment';
import { NotesService } from './../../../api/api-services/item-management/notes.service';

@Component({
  selector: 'app-aprrove-note',
  templateUrl: './aprrove-note.component.html',
  styleUrls: ['./aprrove-note.component.css']
})
export class AprroveNoteComponent implements OnInit, OnDestroy {

  noteItem;
  processing = false;
  reviews;
  @Input()
  set note(note) {
    if (note) {
      this.noteItem = note.note;
      this.reviews = this.notesService.getReviews(this.noteItem.id);
      this.mappingMedia();
      this.toggleModal();
    }
  }
  @Output() onAlterDataEvent = new EventEmitter<any>();
  private alive = true;
  constructor(
    private notesService: NotesService
  ) { }

  ngOnInit() {
  }

  onCancel() {
    this.toggleModal();
    this.noteItem = null;
  }

  onReject() {
    this.processing = true;
    this.notesService.reject(this.noteItem.id)
      .takeWhile(alive => this.alive)
      .subscribe(res => {
        this.onAlterDataEvent.emit(true);
        this.processing = false;
        this.toggleModal();
        this.noteItem = null;
      }, error => {
        this.toggleModal();
        this.noteItem = null;
        this.processing = false;
      });
  }

  onApprove() {
    this.processing = true;
    this.notesService.approve(this.noteItem.id)
      .takeWhile(alive => this.alive)
      .subscribe(res => {
        this.onAlterDataEvent.emit(true);
        this.processing = false;
        this.toggleModal();
        this.noteItem = null;
      }, error => {
        this.toggleModal();
        this.noteItem = null;
        this.processing = false;
      });
  }

  getRatingValue(rating) {
    try {
      if (rating) {
        const value = Object.values(rating);
        if (value && value.length > 0) {
          return value[0];
        }
      }
    } catch (exception) {
      console.log(exception);
    }
  }

  onNext() {
    try {
      if (this.noteItem.medias) {
        let index = this.noteItem.medias.findIndex(media => {
          return media.class === 'active';
        });
        this.noteItem.medias[index].class = 'inactive';
        const nextIndex = index === this.noteItem.medias.length - 1 ? 0 : ++index;
        this.noteItem.medias[nextIndex].class = 'active';
      }
    } catch (exception) {
      console.log(exception);
    }
  }

  onPrevious() {
    try {
      if (this.noteItem.medias) {
        let index = this.noteItem.medias.findIndex(media => {
          return media.class === 'active';
        });
        this.noteItem.medias[index].class = 'inactive';
        const prevIndex = index === 0 ? this.noteItem.medias.length - 1 : --index;
        this.noteItem.medias[prevIndex].class = 'active';
      }
    } catch (exception) {
      console.log(exception);
    }
  }

  onClickImage(index) {
    try {
      const mediaTmp = this.noteItem.medias.find(media => {
        return media.class === 'active';
      });
      mediaTmp.class = 'inactive';
      this.noteItem.medias[index].class = 'active';
    } catch (exception) {
      console.log(exception);
    }
  }

  private toggleModal() {
    try {
      $('#approve-note-modal').modal('toggle');
    } catch (exception) {
      console.log(exception);
    }
  }

  private mappingMedia() {
    try {
      const media = this.noteItem.media;
      if (media) {
        this.noteItem.medias = [];
        Object.keys(media).forEach((key, index) => {
          const classActive = index === 0 ? 'active' : 'inactive';
          const audioPath = media[key].audio ? `${environment.appResourceServer}audio/${media[key].audio}` : null;
          this.noteItem.medias.push({
            image: `${environment.appResourceServer}images/${media[key].image}`,
            audio: audioPath,
            class: classActive,
            index: index + 1
          });
        });
      }
    } catch (exception) {
      console.log(exception);
    }
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
