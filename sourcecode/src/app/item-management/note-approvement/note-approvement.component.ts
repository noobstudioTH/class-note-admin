import { Component, OnInit, OnDestroy } from '@angular/core';

import { Notes } from './../../enum/notes.enum';
import { NotesService } from './../../api/api-services/item-management/notes.service';
import { StatusService } from './../../api/api-services/master/status.service';

@Component({
  selector: 'app-note-approvement',
  templateUrl: './note-approvement.component.html',
  styleUrls: ['./note-approvement.component.css']
})
export class NoteApprovementComponent implements OnInit, OnDestroy {

  dataService;
  columnList = [{ prop: 'status', name: 'Status' }, { prop: 'addedDate', name: 'Date Added' },
  { prop: 'id', name: 'Note ID' }, { prop: 'name', name: 'Note Name' },
  { prop: 'writername', name: 'Writer Name' }, { name: 'Action' }];
  formConfig = [
    [{ id: Notes.noteId, name: 'Note Id', type: 'text', placeholder: 'Note Id' },
    { id: Notes.noteName, name: 'Note Name', type: 'text', placeholder: 'Note Name' }],
    [{ id: Notes.writerName, name: 'Writer Name', type: 'text', placeholder: 'Writer Name' },
    { id: Notes.status, name: 'Status', type: 'dropdown', optionList: this.statusService.get() }]
  ];
  searchForm;
  selectedNote;
  onAlterData = { flag: false };
  private alive = true;

  constructor(
    private notesService: NotesService,
    private statusService: StatusService
  ) {
    this.dataService = this.notesService;
  }

  ngOnInit() { }

  onsubmitEvent($event) {
    try {
      this.searchForm = { form: $event };
    } catch (exception) {
      console.log(exception);
    }
  }

  onTableRowEdit($event) {
    this.selectedNote = { note: $event };
  }

  onAlterDataEvent($event) {
    this.onAlterData = { flag: true };
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
