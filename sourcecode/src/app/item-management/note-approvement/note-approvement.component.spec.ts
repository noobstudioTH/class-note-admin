import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoteApprovementComponent } from './note-approvement.component';

describe('NoteApprovementComponent', () => {
  let component: NoteApprovementComponent;
  let fixture: ComponentFixture<NoteApprovementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoteApprovementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoteApprovementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
