import { LevelsService } from './../../../api/api-services/master/levels.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-difficulty',
  templateUrl: './difficulty.component.html',
  styleUrls: ['./difficulty.component.css']
})
export class DifficultyComponent implements OnInit, OnDestroy {

  dataService;
  columnList = [{ prop: 'text', name: 'Categories Name' }, { prop: 'selected', name: '#Selected' }, { name: 'Action' }];
  addDifficulty;
  updateDifficulty;
  selectedRow;
  onAlterData = { flag: false };
  private alive = true;
  constructor(
    private levelsService: LevelsService
  ) {
    this.dataService = this.levelsService;
  }

  ngOnInit() {
  }

  onAdd() {
    this.addDifficulty = {};
  }

  onDelete() {
    this.levelsService.delete(this.selectedRow)
      .takeWhile(alive => this.alive)
      .subscribe(res => {
        this.onAlterDataEvent(null);
      });
  }

  onTableRowEdit($event) {
    this.updateDifficulty = JSON.parse(JSON.stringify($event));
  }

  onTableRowSelect($event) {
    this.selectedRow = $event;
  }

  onAlterDataEvent($event) {
    this.onAlterData = { flag: true };
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
