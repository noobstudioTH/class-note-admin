import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateDifficultyComponent } from './update-difficulty.component';

describe('UpdateDifficultyComponent', () => {
  let component: UpdateDifficultyComponent;
  let fixture: ComponentFixture<UpdateDifficultyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateDifficultyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateDifficultyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
