declare const $: any;

import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { LevelsService } from './../../../../api/api-services/master/levels.service';

@Component({
  selector: 'app-add-difficulty',
  templateUrl: './add-difficulty.component.html',
  styleUrls: ['./add-difficulty.component.css']
})
export class AddDifficultyComponent implements OnInit, OnDestroy {

  subjectForm;
  processing;
  errorMessage;
  @Input()
  set inputData(inputData) {
    if (inputData) {
      this.createForm();
    }
  }
  @Output() onAlterDataEvent = new EventEmitter<any>();
  private alive = true;
  constructor(
    private levelsService: LevelsService
  ) { }

  ngOnInit() {
  }

  onAdd() {
    this.processing = true;
    this.levelsService.create(this.subjectForm.get('code').value, this.subjectForm.get('name').value)
      .takeWhile(alive => this.alive)
      .subscribe(res => {
        this.onAlterDataEvent.emit(true);
        this.toggleModal();
        this.processing = false;
      }, error => {
        this.errorMessage = error.error.errors;
        this.processing = false;
      });
  }

  private toggleModal() {
    $('#add-difficulty-modal').modal('toggle');
  }

  private createForm() {
    this.errorMessage = null;
    this.processing = false;
    this.subjectForm = new FormBuilder().group({
      code: [null, Validators.required],
      name: [null, Validators.required]
    });
    this.toggleModal();
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
