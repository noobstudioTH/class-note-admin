declare const $: any;

import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { SubjectsService } from './../../../../api/api-services/master/subjects.service';

@Component({
  selector: 'app-add-subject',
  templateUrl: './add-subject.component.html',
  styleUrls: ['./add-subject.component.css']
})
export class AddSubjectComponent implements OnInit, OnDestroy {

  subjectForm;
  processing;
  errorMessage;
  @Input()
  set inputData(inputData) {
    if (inputData) {
      this.createForm();
    }
  }
  @Output() onAlterDataEvent = new EventEmitter<any>();
  private alive = true;
  constructor(
    private subjectsService: SubjectsService
  ) { }

  ngOnInit() {
  }

  onAdd() {
    this.processing = true;
    this.subjectsService.create(this.subjectForm.get('code').value, this.subjectForm.get('name').value)
      .takeWhile(alive => this.alive)
      .subscribe(res => {
        this.onAlterDataEvent.emit(true);
        this.toggleModal();
        this.processing = false;
      }, error => {
        this.errorMessage = error.error.errors;
        this.processing = false;
      });
  }

  private toggleModal() {
    $('#add-subject-modal').modal('toggle');
  }

  private createForm() {
    this.errorMessage = null;
    this.processing = false;
    this.subjectForm = new FormBuilder().group({
      code: [null, Validators.required],
      name: [null, Validators.required]
    });
    this.toggleModal();
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
