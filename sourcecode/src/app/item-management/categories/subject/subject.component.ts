import { Component, OnInit, OnDestroy } from '@angular/core';

import { SubjectsService } from './../../../api/api-services/master/subjects.service';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.css']
})
export class SubjectComponent implements OnInit, OnDestroy {

  dataService;
  columnList = [{ prop: 'text', name: 'Categories Name' }, { prop: 'selected', name: '#Selected' }, { name: 'Action' }];
  addSubject;
  updateSubject;
  selectedRow;
  onAlterData = { flag: false };
  private alive = true;
  constructor(
    private subjectsService: SubjectsService
  ) {
    this.dataService = this.subjectsService;
  }

  ngOnInit() {
  }

  onAdd() {
    this.addSubject = {};
  }

  onDelete() {
    this.subjectsService.delete(this.selectedRow)
      .takeWhile(alive => this.alive)
      .subscribe(res => {
        this.onAlterDataEvent(null);
      });
  }

  onTableRowEdit($event) {
    this.updateSubject = JSON.parse(JSON.stringify($event));
  }

  onTableRowSelect($event) {
    this.selectedRow = $event;
  }

  onAlterDataEvent($event) {
    this.onAlterData = { flag: true };
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
