declare const $: any;

import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { SubjectsService } from './../../../../api/api-services/master/subjects.service';

@Component({
  selector: 'app-update-subject',
  templateUrl: './update-subject.component.html',
  styleUrls: ['./update-subject.component.css']
})
export class UpdateSubjectComponent implements OnInit, OnDestroy {

  subjectForm;
  processing;
  errorMessage;
  @Input()
  set inputData(inputData) {
    if (inputData) {
      this.createForm(inputData);
    }
  }
  @Output() onAlterDataEvent = new EventEmitter<any>();
  private alive = true;
  constructor(
    private subjectsService: SubjectsService
  ) { }

  ngOnInit() {
  }

  onUpdate() {
    this.processing = true;
    this.subjectsService.update(this.subjectForm.get('code').value, this.subjectForm.get('name').value)
      .takeWhile(alive => this.alive)
      .subscribe(res => {
        this.onAlterDataEvent.emit(true);
        this.toggleModal();
        this.processing = false;
      }, error => {
        this.processing = false;
      });
  }

  private toggleModal() {
    $('#update-subject-modal').modal('toggle');
  }

  private createForm(inputData) {
    this.errorMessage = null;
    this.processing = false;
    this.subjectForm = new FormBuilder().group({
      code: [inputData.code, Validators.required],
      name: [inputData.text, Validators.required]
    });
    this.subjectForm.get('code').disable();
    this.toggleModal();
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
