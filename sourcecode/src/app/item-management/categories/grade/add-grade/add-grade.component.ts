declare const $: any;

import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { GradesService } from './../../../../api/api-services/master/grades.service';

@Component({
  selector: 'app-add-grade',
  templateUrl: './add-grade.component.html',
  styleUrls: ['./add-grade.component.css']
})
export class AddGradeComponent implements OnInit, OnDestroy {

  subjectForm;
  processing;
  errorMessage;
  @Input()
  set inputData(inputData) {
    if (inputData) {
      this.createForm();
    }
  }
  @Output() onAlterDataEvent = new EventEmitter<any>();
  private alive = true;
  constructor(
    private gradesService: GradesService
  ) { }

  ngOnInit() {
  }

  onAdd() {
    this.processing = true;
    this.gradesService.create(this.subjectForm.get('code').value, this.subjectForm.get('name').value)
      .takeWhile(alive => this.alive)
      .subscribe(res => {
        this.onAlterDataEvent.emit(true);
        this.toggleModal();
        this.processing = false;
      }, error => {
        this.errorMessage = error.error.errors;
        this.processing = false;
      });
  }

  private toggleModal() {
    $('#add-grade-modal').modal('toggle');
  }

  private createForm() {
    this.errorMessage = null;
    this.processing = false;
    this.subjectForm = new FormBuilder().group({
      code: [null, Validators.required],
      name: [null, Validators.required]
    });
    this.toggleModal();
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
