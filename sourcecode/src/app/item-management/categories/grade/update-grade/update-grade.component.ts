declare const $: any;

import { OnInit, Input, OnDestroy, Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { GradesService } from './../../../../api/api-services/master/grades.service';

@Component({
  selector: 'app-update-grade',
  templateUrl: './update-grade.component.html',
  styleUrls: ['./update-grade.component.css']
})
export class UpdateGradeComponent implements OnInit, OnDestroy {

  subjectForm;
  processing;
  errorMessage;
  @Input()
  set inputData(inputData) {
    if (inputData) {
      this.createForm(inputData);
    }
  }
  @Output() onAlterDataEvent = new EventEmitter<any>();
  private alive = true;
  constructor(
    private gradesService: GradesService
  ) { }

  ngOnInit() {
  }

  onUpdate() {
    this.processing = true;
    this.gradesService.update(this.subjectForm.get('code').value, this.subjectForm.get('name').value)
      .takeWhile(alive => this.alive)
      .subscribe(res => {
        this.onAlterDataEvent.emit(true);
        this.toggleModal();
        this.processing = false;
      }, error => {
        this.processing = false;
      });
  }

  private toggleModal() {
    $('#update-grade-modal').modal('toggle');
  }

  private createForm(inputData) {
    this.errorMessage = null;
    this.processing = false;
    this.subjectForm = new FormBuilder().group({
      code: [inputData.code, Validators.required],
      name: [inputData.text, Validators.required]
    });
    this.subjectForm.get('code').disable();
    this.toggleModal();
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
