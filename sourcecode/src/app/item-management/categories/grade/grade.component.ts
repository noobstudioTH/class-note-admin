import { Component, OnInit, OnDestroy } from '@angular/core';

import { GradesService } from './../../../api/api-services/master/grades.service';

@Component({
  selector: 'app-grade',
  templateUrl: './grade.component.html',
  styleUrls: ['./grade.component.css']
})
export class GradeComponent implements OnInit, OnDestroy {

  dataService;
  columnList = [{ prop: 'text', name: 'Categories Name' }, { prop: 'selected', name: '#Selected' }, { name: 'Action' }];
  addGrade;
  updateGrade;
  selectedRow;
  onAlterData = { flag: false };
  private alive = true;
  constructor(
    private gradesService: GradesService
  ) {
    this.dataService = this.gradesService;
  }

  ngOnInit() {
  }

  onAdd() {
    this.addGrade = {};
  }

  onDelete() {
    this.gradesService.delete(this.selectedRow)
      .takeWhile(alive => this.alive)
      .subscribe(res => {
        this.onAlterDataEvent(null);
      });
  }

  onTableRowEdit($event) {
    this.updateGrade = JSON.parse(JSON.stringify($event));
  }

  onTableRowSelect($event) {
    this.selectedRow = $event;
  }

  onAlterDataEvent($event) {
    this.onAlterData = { flag: true };
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
