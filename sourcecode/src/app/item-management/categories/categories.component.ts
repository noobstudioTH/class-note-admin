import { Component, OnInit, ViewChild, ComponentFactoryResolver } from '@angular/core';

import { DynamicDirective } from '../directive/dynamic.directive';
import { SubjectComponent } from './subject/subject.component';
import { DifficultyComponent } from './difficulty/difficulty.component';
import { GradeComponent } from './grade/grade.component';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  @ViewChild(DynamicDirective) dynamic: DynamicDirective;
  constructor(
    private componentFactoryResolver: ComponentFactoryResolver
  ) { }
  menus = [
    { name: 'Subject', component: SubjectComponent, selected: true },
    { name: 'Grade', component: GradeComponent, selected: false },
    { name: 'Difficulty', component: DifficultyComponent, selected: false }
  ];
  ngOnInit() {
    this.loadComponent(SubjectComponent);
  }

  onMenuClick(menu) {
    try {
      this.menus.map(_menu => {
        return _menu.selected = false;
      });
      menu.selected = true;
      this.loadComponent(menu.component);
    } catch (exception) {
      console.log(exception);
    }
  }

  private loadComponent(component) {
    try {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
      const viewContainerRef = this.dynamic.viewContainerRef;
      viewContainerRef.clear();
      const componentRef = viewContainerRef.createComponent(componentFactory);
    } catch (exception) {
      console.log(exception);
    }
  }

}
