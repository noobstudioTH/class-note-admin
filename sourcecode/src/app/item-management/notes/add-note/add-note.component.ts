declare const $;

import { FormGroup, Validators, FormBuilder, AbstractControl, FormControl } from '@angular/forms';
import { Component, OnInit, OnDestroy, Input, EventEmitter, Output } from '@angular/core';

import { FileUploader } from 'ng2-file-upload';

import { NoteItem } from './../../../enum/notes.enum';
import { NotesService } from './../../../api/api-services/item-management/notes.service';
import { environment } from './../../../../environments/environment';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.css']
})
export class AddNoteComponent implements OnInit, OnDestroy {

  @Input()
  set note(note) {
    if (note) {
      this.initialData();
    }
  }
  currentStep;
  wizardConfig;
  buttonConfigList;
  processing: boolean;
  fileUploader: FileUploader;
  audioUploader;
  descriptionForm: FormGroup;
  @Output() onAlterDataEvent = new EventEmitter<any>();
  private alive = true;
  constructor(
    private notesService: NotesService,
  ) { }

  ngOnInit() {
  }

  onNext(buttonConfig) {
    if (this.currentStep === 1) {
      if (this.descriptionForm.get(NoteItem.key).value) {
        this.updateNote();
      } else {
        this.createNote();
      }
    } else if (this.currentStep === 2) {
      if (buttonConfig.id === 1) {
        this.toggleModal();
        this.onAlterDataEvent.emit(true);
      } else {
        this.buttonConfigList[1].show = false;
        this.currentStep = 3;
        this.fileUploader.queue = this.fileUploader.queue.filter(item => {
          this.intialAudioUploader(item);
          return item.isSuccess;
        });
        this.audioUploader = { fileUploader: this.fileUploader };
      }
    } else {
      this.toggleModal();
      this.onAlterDataEvent.emit(true);
    }
  }

  onCancel() {
    if (this.descriptionForm.get(NoteItem.key).value) {
      this.deleteNote();
    }
    this.toggleModal();
  }

  onStepClick(step) {
    if (step < this.currentStep) { this.currentStep = step; }
    if (this.currentStep === 1) {
      this.buttonConfigList[0].show = false;
      this.buttonConfigList[1].show = true;
      this.buttonConfigList[1].disabled = this.descriptionForm.invalid;
    } else if (this.currentStep === 2) {
      this.buttonConfigList[0].show = true;
      this.buttonConfigList[0].disabled = false;
      this.buttonConfigList[1].show = true;
      this.buttonConfigList[1].disabled = false;
    }
  }

  onDeleteItem($event) {
    try {
      if ($event.isError) {
        $event.remove();
      } else {
        this.processing = true;
        this.notesService.deleteFile(this.descriptionForm.get(NoteItem.key).value, $event.file.media_id)
          .takeWhile(alive => this.alive)
          .subscribe(response => {
            $event.remove();
            this.processing = false;
          }, error => {
            this.processing = false;
          });
      }
    } catch (exception) {
      console.log(exception);
    }
  }

  onDeleteFileAudioItem($event) {
    try {
      if ($event.audioUploader.queue[0].isError) {
        $event.audioUploader.queue[0].remove();
      } else {
        this.processing = true;
        this.notesService.deleteAudio(this.descriptionForm.get(NoteItem.key).value, $event.media_id)
          .takeWhile(alive => this.alive)
          .subscribe(response => {
            $event.audioUploader.queue[0].remove();
            this.processing = false;
          }, error => {
            this.processing = false;
          });
      }
    } catch (exception) {
      console.log(exception);
    }
  }

  private toggleModal() {
    try {
      $('#add-note-modal').modal('toggle');
    } catch (exception) {
      console.log(exception);
    }
  }

  private initialData() {
    this.currentStep = 1;
    this.wizardConfig = [
      { step: 1, name: 'Description' },
      { step: 2, name: 'Upload' },
      { step: 3, name: 'Audio' }
    ];
    this.buttonConfigList = [{ id: 1, name: 'Submit', disabled: true, show: false },
    { id: 2, name: 'Next', disabled: true, show: true }];
    this.processing = false;
    this.fileUploader = null;
    this.createDescriptionForm();
    this.toggleModal();
  }

  private createDescriptionForm() {
    this.descriptionForm = new FormBuilder().group({
      key: [null],
      caption: [null, Validators.required],
      keywords: [null, Validators.required],
      subject: [null, Validators.required],
      grade: [null, Validators.required],
      level: [null, Validators.required],
      privacy: [null, Validators.required],
      series_id: [null, Validators.required]
    });
    this.descriptionForm.valueChanges.subscribe(data => {
      this.buttonConfigList[1].disabled = this.descriptionForm.invalid;
    });
  }

  private createNote() {
    this.processing = true;
    this.notesService.create(this.descriptionForm)
      .takeWhile(alive => this.alive)
      .subscribe(response => {
        try {
          this.currentStep = 2;
          this.buttonConfigList[0].show = true;
          this.descriptionForm.get(NoteItem.key).setValue(response.data.key);
          this.initialImageUploder();
        } catch (exception) {
          console.log(exception);
        }
        this.processing = false;
      }, error => {
        this.processing = false;
      });
  }

  private updateNote() {
    this.processing = true;
    this.notesService.update(this.descriptionForm)
      .takeWhile(alive => this.alive)
      .subscribe(response => {
        try {
          this.currentStep = 2;
          this.initialImageUploder();
          this.buttonConfigList[0].show = true;
          this.checkButtonConfig();
        } catch (exception) {
          console.log(exception);
        }
        this.processing = false;
      }, error => {
        this.processing = false;
      });
  }

  private deleteNote() {
    this.notesService.delete(this.descriptionForm.get(NoteItem.key).value)
      .takeWhile(alive => this.alive)
      .subscribe(data => {
        this.onAlterDataEvent.emit(true);
      });
  }

  private checkButtonConfig() {
    try {
      this.processing = false;
      const isSuccess = this.fileUploader.queue.some(item => {
        return item.isSuccess;
      });
      if (isSuccess) {
        this.buttonConfigList[0].disabled = false;
        this.buttonConfigList[1].disabled = false;
      } else {
        this.buttonConfigList[0].disabled = true;
        this.buttonConfigList[1].disabled = true;
      }
    } catch (exception) {
      console.log(exception);
    }
  }

  private onSuccessItem(item, response, status, headers) {
    try {
      const data = JSON.parse(response).data;
      item.file.media_id = data.media_id;
    } catch (exception) {
      console.log(exception);
    }
  }

  private initialImageUploder() {
    if (!this.fileUploader) {
      const url = `${environment.appApiServer}notes/${this.descriptionForm.get(NoteItem.key).value}/media`;
      this.fileUploader = new FileUploader({
        url: url, autoUpload: true, maxFileSize: (1024 * 1024 * 10), allowedFileType: ['image']
        , itemAlias: 'image'
      });
      this.buttonConfigList[1].disabled = true;
      this.fileUploader.onProgressAll = () => { this.processing = true; };
      this.fileUploader.onCompleteAll = () => this.checkButtonConfig();
      this.fileUploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);
    }
  }

  private intialAudioUploader(fileItem) {
    try {
      if (!fileItem.file['audioUploader']) {
        fileItem.file['audioUploader'] = new FileUploader({
          url: `${environment.appApiServer}notes/${this.descriptionForm.get(NoteItem.key).value}/media/${fileItem.file['media_id']}/audio`,
          autoUpload: true, maxFileSize: (1024 * 1024 * 10), allowedFileType: ['audio']
          , itemAlias: 'audio', queueLimit: 1
        });
        fileItem.file['audioUploader'].onProgressAll = () => { this.processing = true; };
        fileItem.file['audioUploader'].onCompleteAll = () => { this.processing = false; };
      }
    } catch (exception) {
      console.log(exception);
    }
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
