import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { v4 as uuid } from 'uuid';
import { FileUploader } from 'ng2-file-upload';

@Component({
  selector: 'app-note-audio',
  templateUrl: './note-audio.component.html',
  styleUrls: ['./note-audio.component.css']
})
export class NoteAudioComponent implements OnInit {

  _fileUploader;
  errorMessage;
  @Input()
  set fileUploader(fileUploader) {
    if (fileUploader) {
      this._fileUploader = fileUploader.fileUploader;
      this._fileUploader.queue.forEach(fileItem => {
        fileItem.file.audioUploader.onWhenAddingFileFailed = (item, filter, options) => this.onWhenAddingFileFailed(item, filter, options);
        fileItem.file.audioUploader.onSuccessItem = (item, response, status, headers) =>
          this.onUploadItem(item, response, status, headers);
        fileItem.file.audioUploader.onErrorItem = (item, response, status, headers) =>
          this.onUploadItem(item, response, status, headers);
        fileItem.file.audioUploader.onAfterAddingFile = (item) => this.onAfterAddingFile(item);
      });
    }
  }
  @Output() onDeleteFileAudioItem = new EventEmitter();
  constructor(
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
  }

  onDeleteFileItem(fileItem) {
    this.onDeleteFileAudioItem.emit(fileItem);
  }

  private onAfterAddingFile(item) {
    item.id = uuid();
    this.errorMessage = null;
  }

  private onWhenAddingFileFailed(item, filter, options) {
    if (filter.name === 'fileType') {
      this.errorMessage = 'Not Allow File Type';
    } else if (filter.name === 'fileSize') {
      this.errorMessage = 'File size is over';
    } else if (filter.name === 'queueLimit') {
      this.errorMessage = 'Cannot upload more than ' + options.queueLimit + ' file(s)';
    }
  }

  private onUploadItem(item, response, status, headers) {
    try {
      new Promise(resolve => {
        const fileReader = new FileReader();
        fileReader.readAsArrayBuffer(item.file.rawFile);
        fileReader.onload = function () {
          const arrayBuffer = fileReader.result;
          const bytes = new Uint8Array(arrayBuffer);
          resolve(bytes);
        };
      }).then(bytes => {
        const blob = new Blob([bytes], { type: item.file.type });
        const blobUrl = this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(blob));
        item.file.url = blobUrl;
        (<HTMLAudioElement>document.getElementById(item.id)).load();
      }).catch(error => {
        console.log(error);
      });
    } catch (exception) {
      console.log(exception);
    }
  }

}
