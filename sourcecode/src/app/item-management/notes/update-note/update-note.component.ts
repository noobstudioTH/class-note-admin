declare const $;

import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { FileUploader } from 'ng2-file-upload';

import { NoteItem } from '../../../enum/notes.enum';
import { NotesService } from './../../../api/api-services/item-management/notes.service';
import { environment } from './../../../../environments/environment';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-update-note',
  templateUrl: './update-note.component.html',
  styleUrls: ['./update-note.component.css']
})
export class UpdateNoteComponent implements OnInit, OnDestroy {

  noteItem;
  @Input()
  set note(note) {
    if (note) {
      this.noteItem = note.note;
      this.initialData();
    }
  }
  currentStep;
  wizardConfig;
  buttonConfigList;
  processing: boolean;
  processingDelete: boolean;
  fileUploader: FileUploader;
  audioUploader;
  descriptionForm: FormGroup;
  @Output() onAlterDataEvent = new EventEmitter<any>();
  private alive = true;
  constructor(
    private notesService: NotesService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
  }

  onNext(buttonConfig) {
    if (this.currentStep === 1) {
      this.updateNote();
    } else {
      this.onAlterDataEvent.emit(true);
      this.toggleModal();
    }
  }

  onStepClick(step) {
    if (step < this.currentStep) { this.currentStep = step; }
    if (this.currentStep === 1) {
      this.buttonConfigList[0].show = false;
      this.buttonConfigList[1].show = true;
      this.buttonConfigList[1].disabled = this.descriptionForm.invalid;
    }
  }

  onDeleteExistingItem($event) {
    this.processing = true;
    this.notesService.deleteFile(this.noteItem.id, $event.id)
      .takeWhile(alive => this.alive)
      .subscribe(response => {
        this.processing = false;
        try {
          $event.remove();
        } catch (exception) { }
      }, error => {
        this.processing = false;
      });
  }

  onDeleteAudioItem($event) {
    this.processing = true;
    this.notesService.deleteAudio(this.noteItem.id, $event.id)
      .takeWhile(alive => this.alive)
      .subscribe(response => {
        if ($event.audio) {
          $event.audio.path = null;
          $event.audio.hasAudio = false;
        }
        if ($event.audio && $event.audio.uploader.queue[0]) {
          $event.audio.uploader.queue[0].remove();
        }
        if ($event.uploader && $event.uploader.queue[0]) {
          $event.uploader.queue[0].remove();
        }
        this.processing = false;
      }, error => {
        this.processing = false;
      });
  }

  onDeleteNote() {
    this.processingDelete = true;
    this.notesService.delete(this.noteItem.id)
      .takeWhile(alive => this.alive)
      .subscribe(response => {
        this.processingDelete = false;
        try {
          this.onAlterDataEvent.emit(true);
          this.toggleModal();
        } catch (exception) { }
      }, error => {
        this.processingDelete = false;
      });
  }

  private toggleModal() {
    try {
      $('#update-note-modal').modal('toggle');
    } catch (exception) {
      console.log(exception);
    }
  }

  private initialData() {
    this.currentStep = 1;
    this.wizardConfig = [
      { step: 1, name: 'Description' },
      { step: 2, name: 'Media' }
    ];
    this.buttonConfigList = [{ id: 1, name: 'Submit', disabled: true, show: false },
    { id: 2, name: 'Next', disabled: true, show: true }];
    this.processing = false;
    this.fileUploader = null;
    this.createDescriptionForm();
    this.toggleModal();
  }

  private createDescriptionForm() {
    this.descriptionForm = new FormBuilder().group({
      key: [this.noteItem.id],
      caption: [this.noteItem.caption, Validators.required],
      keywords: [this.mappingKeywords(), Validators.required],
      subject: [this.noteItem.subject, Validators.required],
      grade: [this.noteItem.grade, Validators.required],
      level: [this.noteItem.level, Validators.required],
      privacy: [this.noteItem.privacy, Validators.required],
    });
    this.buttonConfigList[1].disabled = this.descriptionForm.invalid;
    this.descriptionForm.valueChanges.subscribe(data => {
      this.buttonConfigList[1].disabled = this.descriptionForm.invalid;
    });
  }

  private mappingKeywords() {
    const keywords = [];
    try {
      this.noteItem.keywords.forEach(keyword => {
        keywords.push({ display: keyword, value: keyword });
      });
    } catch (exception) {
      console.log(exception);
    } finally {
      return keywords;
    }
  }

  private updateNote() {
    this.processing = true;
    this.notesService.update(this.descriptionForm)
      .takeWhile(alive => this.alive)
      .subscribe(response => {
        try {
          this.currentStep = 2;
          this.initialImageUploder();
          this.buttonConfigList[0].show = true;
          this.buttonConfigList[1].show = false;
          this.checkButtonConfig();
          this.onAlterDataEvent.emit(true);
        } catch (exception) {
          console.log(exception);
        }
        this.processing = false;
      }, error => {
        this.processing = false;
      });
  }

  private initialImageUploder() {
    if (!this.fileUploader) {
      const url = `${environment.appApiServer}notes/${this.descriptionForm.get(NoteItem.key).value}/media`;
      this.fileUploader = new FileUploader({
        url: url, autoUpload: true, maxFileSize: (1024 * 1024 * 10), allowedFileType: ['image']
        , itemAlias: 'image'
      });
      this.buttonConfigList[1].disabled = true;
      this.fileUploader.onProgressAll = () => { this.processing = true; };
      this.fileUploader.onCompleteAll = () => this.checkButtonConfig();
      this.fileUploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);
      this.fileUploader.onErrorItem = (item, response, status, headers) => item.remove();
      this.fileUploader['existing'] = this.mappingMediaUrl();
    }
  }

  private checkButtonConfig() {
    try {
      this.processing = false;
      if (this.fileUploader.queue.length > 0 || this.fileUploader['existing'].length > 0) {
        this.buttonConfigList[0].disabled = false;
      } else {
        this.buttonConfigList[0].disabled = true;
      }
    } catch (exception) {
      console.log(exception);
    }
  }

  private onSuccessItem(item, response, status, headers) {
    try {
      const data = JSON.parse(response).data;
      item.id = data.media_id;
      new Promise(resolve => {
        const fileReader = new FileReader();
        fileReader.readAsArrayBuffer(item.file.rawFile);
        fileReader.onload = function () {
          const arrayBuffer = fileReader.result;
          const bytes = new Uint8Array(arrayBuffer);
          resolve(bytes);
        };
      }).then(bytes => {
        const blob = new Blob([bytes], { type: item.file.type });
        const blobUrl = this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(blob));
        item.file.url = blobUrl;
        item.uploader = this.intialAudioUploader(data.media_id);
      }).catch(error => {
        console.log(error);
      });
    } catch (exception) {
      console.log(exception);
    }
  }

  private mappingMediaUrl() {
    const media = [];
    try {
      if (this.noteItem.media) {
        Object.keys(this.noteItem.media).forEach(key => {
          media.push({
            id: key,
            image: {
              path: `${environment.appResourceServer}images/${this.noteItem.media[key].image}`
              , name: this.noteItem.media[key].image
            }, audio: {
              path: `${environment.appResourceServer}audio/${this.noteItem.media[key].audio}`
              , name: this.noteItem.media[key].audio, hasAudio: this.noteItem.media[key].audio ? true : false
              , uploader: this.intialAudioUploader(key)
            }
          });
        });
      }
    } catch (exception) {
      console.log(exception);
    } finally {
      return media;
    }
  }

  private intialAudioUploader(mediaId) {
    let audioUploader = null;
    try {
      audioUploader = new FileUploader({
        url: `${environment.appApiServer}notes/${this.descriptionForm.get(NoteItem.key).value}/media/${mediaId}/audio`,
        autoUpload: true, maxFileSize: (1024 * 1024 * 10), allowedFileType: ['audio']
        , itemAlias: 'audio', queueLimit: 1
      });
      audioUploader.onProgressAll = () => { this.processing = true; };
      audioUploader.onCompleteAll = () => { this.processing = false; };
      audioUploader.onSuccessItem = (item, response, status, headers) => this.onUploadItem(item, response, status, headers);
    } catch (exception) {
      console.log(exception);
    } finally {
      return audioUploader;
    }
  }

  private onUploadItem(item, response, status, headers) {
    try {
      new Promise(resolve => {
        const fileReader = new FileReader();
        fileReader.readAsArrayBuffer(item.file.rawFile);
        fileReader.onload = function () {
          const arrayBuffer = fileReader.result;
          const bytes = new Uint8Array(arrayBuffer);
          resolve(bytes);
        };
      }).then(bytes => {
        const blob = new Blob([bytes], { type: item.file.type });
        const blobUrl = this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(blob));
        item.file.url = blobUrl;
        item.file.url.hasAudio = true;
      }).catch(error => {
        console.log(error);
      });
    } catch (exception) {
      console.log(exception);
    }
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
