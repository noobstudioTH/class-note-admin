import { FormGroup } from '@angular/forms';
import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

import { NoteItem } from './../../../enum/notes.enum';
import { GradesService } from './../../../api/api-services/master/grades.service';
import { LevelsService } from './../../../api/api-services/master/levels.service';
import { SubjectsService } from './../../../api/api-services/master/subjects.service';
import { PrivacyService } from './../../../api/api-services/master/privacy.service';
import { SeriesService } from './../../../api/api-services/item-management/series.service';

@Component({
  selector: 'app-note-description',
  templateUrl: './note-description.component.html',
  styleUrls: ['./note-description.component.css']
})
export class NoteDescriptionComponent implements OnInit {

  noteDescform: FormGroup;
  @Input()
  set descriptionForm(descriptionForm) {
    this.noteDescform = descriptionForm;
    if (this.noteDescform && this.noteDescform.get(NoteItem.series_id)) {
      this.masterDataConfig = this.masterConfigAdd;
    } else {
      this.masterDataConfig = this.masterConfigUpdate;
    }
  }

  masterDataConfig = [];

  constructor(
    private subjectsService: SubjectsService,
    private levelsService: LevelsService,
    private gradesService: GradesService,
    private privacyService: PrivacyService,
    private seriesService: SeriesService
  ) { }

  ngOnInit() { }

  get masterConfigAdd() {
    return [{ label: 'Subject', default: 'Select subject', optionList: this.subjectsService.get(), controlName: NoteItem.subject },
    { label: 'Grades', default: 'Select school grade', optionList: this.gradesService.get(), controlName: NoteItem.grade },
    { label: 'Difficulty', default: 'Select difficulty', optionList: this.levelsService.get(), controlName: NoteItem.level },
    { label: 'Privacy', default: 'Select privacy', optionList: this.privacyService.get(), controlName: NoteItem.privacy },
    { label: 'Series', default: 'Select serie', optionList: this.seriesService.getSeriesMaster(), controlName: NoteItem.series_id }];
  }

  get masterConfigUpdate() {
    return [{ label: 'Subject', default: 'Select subject', optionList: this.subjectsService.get(), controlName: NoteItem.subject },
    { label: 'Grades', default: 'Select school grade', optionList: this.gradesService.get(), controlName: NoteItem.grade },
    { label: 'Difficulty', default: 'Select difficulty', optionList: this.levelsService.get(), controlName: NoteItem.level },
    { label: 'Privacy', default: 'Select privacy', optionList: this.privacyService.get(), controlName: NoteItem.privacy }];
  }

}
