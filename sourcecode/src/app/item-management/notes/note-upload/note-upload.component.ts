import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';

import { environment } from './../../../../environments/environment';

@Component({
  selector: 'app-note-upload',
  templateUrl: './note-upload.component.html',
  styleUrls: ['./note-upload.component.css']
})
export class NoteUploadComponent implements OnInit {

  uploader: FileUploader;
  @Input()
  set fileUploader(fileUploader) {
    if (fileUploader) {
      this.uploader = fileUploader;
      this.initialUploder();
    }
  }
  @Output() onDeleteItem = new EventEmitter();
  hasBaseDropZoneOver = false;
  hasAnotherDropZoneOver = false;
  errorMessage: string;

  constructor() { }

  ngOnInit() {
  }

  fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

  onFileDrop($event) { }

  onFileSelected($event) { }

  onDeleteFileItem(item) {
    this.onDeleteItem.emit(item);
  }

  checkDuplicate(fileItem) {
    try {
      this.errorMessage = null;
      this.uploader.queue.forEach(queue => {
        const duplicated = this.uploader.queue.filter(check => {
          return queue.file.name === check.file.name;
        });
        if (duplicated.length > 1) {
          this.errorMessage = 'Duplicated File';
          fileItem.remove();
          return;
        }
      });
    } catch (exception) {
      console.log(exception);
    }
  }

  private initialUploder() {
    this.uploader.onAfterAddingFile = (fileItem) => { this.onAfterAddingFile(fileItem); };
    this.uploader.onWhenAddingFileFailed = (item, filter, options) => this.onWhenAddingFileFailed(item, filter, options);
  }

  private onWhenAddingFileFailed(item, filter, options) {
    console.log(filter);
    if (filter.name === 'fileType') {
      this.errorMessage = 'Not Allow File Type';
    } else if (filter.name === 'fileSize') {
      this.errorMessage = 'File size is over';
    }
  }

  private onAfterAddingFile(fileItem) {
    try {
      this.checkDuplicate(fileItem);
    } catch (exception) {
      console.log(exception);
    }
  }

}
