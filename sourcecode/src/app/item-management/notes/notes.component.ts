import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Notes } from './../../enum/notes.enum';
import { NotesService } from './../../api/api-services/item-management/notes.service';
import { PrivacyService } from './../../api/api-services/master/privacy.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit, OnDestroy {

  dataService;
  columnList = [{ prop: 'id', name: 'Note ID' }, { prop: 'name', name: 'Note Name' },
  { prop: 'writername', name: 'Writer Name' }, { prop: 'privacy', name: 'Status' }, { name: 'Action' }];
  formConfig = [
    [{ id: Notes.noteId, name: 'Note Id', type: 'text', placeholder: 'Note Id' },
    { id: Notes.noteName, name: 'Note Name', type: 'text', placeholder: 'Note Name' }],
    [{ id: Notes.writerName, name: 'Writer Name', type: 'text', placeholder: 'Writer Name' },
    { id: Notes.privacy, name: 'Status', type: 'dropdown', optionList: this.privacyService.get() }]
  ];
  searchForm;
  addNote;
  selectedNote;
  onAlterData = { flag: false };
  private alive = true;

  constructor(
    private notesService: NotesService,
    private privacyService: PrivacyService
  ) {
    this.dataService = this.notesService;
  }

  ngOnInit() { }

  onsubmitEvent($event) {
    try {
      this.searchForm = { form: $event };
    } catch (exception) {
      console.log(exception);
    }
  }

  onAddNote() {
    this.addNote = {};
  }

  onTableRowEdit($event) {
    this.selectedNote = { note: JSON.parse(JSON.stringify($event)) };
  }

  onAlterDataEvent($event) {
    this.onAlterData = { flag: true };
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
