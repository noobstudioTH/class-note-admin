import { Routes, RouterModule } from '@angular/router';
import { NgModule, Component } from '@angular/core';

import { NotesComponent } from './notes/notes.component';
import { SeriesComponent } from './series/series.component';
import { CategoriesComponent } from './categories/categories.component';
import { NoteApprovementComponent } from './note-approvement/note-approvement.component';
import { NoteReportComponent } from './note-report/note-report.component';

const itemManagementRoutes: Routes = [
    { path: 'notes', component: NotesComponent },
    { path: 'series', component: SeriesComponent },
    { path: 'categories', component: CategoriesComponent },
    { path: 'note-approvement', component: NoteApprovementComponent },
    { path: 'note-report', component: NoteReportComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(itemManagementRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ItemManagementRoutingModule { }
