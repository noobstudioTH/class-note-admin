import { Component, OnInit, OnDestroy } from '@angular/core';

import { Notes } from '../../enum/notes.enum';
import { ReportService } from './../../api/api-services/item-management/report.service';
import { StatusService } from './../../api/api-services/master/status.service';

@Component({
  selector: 'app-note-report',
  templateUrl: './note-report.component.html',
  styleUrls: ['./note-report.component.css']
})
export class NoteReportComponent implements OnInit, OnDestroy {

  dataService;
  columnList = [{ prop: 'reportedNumber', name: '#Reported' }, { prop: 'id', name: 'Note ID' },
  { prop: 'name', name: 'Note Name' }, { prop: 'addedDate', name: 'Lasted Date' },
  { prop: 'status', name: 'Status' }, { name: 'Action' }];
  formConfig = [
    [{ id: Notes.noteId, name: 'Note Id', type: 'text', placeholder: 'Note Id' },
    { id: Notes.noteName, name: 'Note Name', type: 'text', placeholder: 'Note Name' }],
    [{ id: Notes.writerName, name: 'Writer Name', type: 'text', placeholder: 'Writer Name' },
    { id: Notes.status, name: 'Status', type: 'dropdown', optionList: this.statusService.get() }]
  ];
  searchForm;
  reports;
  onAlterData = { flag: false };
  private alive = true;

  constructor(
    private reportService: ReportService,
    private statusService: StatusService
  ) {
    this.dataService = this.reportService;
  }

  ngOnInit() { }

  onsubmitEvent($event) {
    try {
      this.searchForm = { form: $event };
    } catch (exception) {
      console.log(exception);
    }
  }

  onTableRowEdit($event) {
    this.reports = JSON.parse(JSON.stringify($event));
  }

  onAlterDataEvent($event) {
    this.onAlterData = { flag: true };
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
