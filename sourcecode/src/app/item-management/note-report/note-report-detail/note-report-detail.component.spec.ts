import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoteReportDetailComponent } from './note-report-detail.component';

describe('NoteReportDetailComponent', () => {
  let component: NoteReportDetailComponent;
  let fixture: ComponentFixture<NoteReportDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoteReportDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoteReportDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
