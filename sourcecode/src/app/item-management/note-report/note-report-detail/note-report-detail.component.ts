declare const $;

import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';

import { NotesService } from './../../../api/api-services/item-management/notes.service';

@Component({
  selector: 'app-note-report-detail',
  templateUrl: './note-report-detail.component.html',
  styleUrls: ['./note-report-detail.component.css']
})
export class NoteReportDetailComponent implements OnInit, OnDestroy {

  columnList = [{ prop: 'index', name: '#' }, { prop: 'message', name: 'Issue' },
  { prop: 'date', name: 'Date' }, { prop: 'time', name: 'Time' }];
  reportDetails;
  processing = false;
  @Input()
  set reports(reports) {
    if (reports) {
      this.reportDetails = reports;
      this.toggleModal();
    }
  }
  @Output() onAlterDataEvent = new EventEmitter<any>();
  private alive = true;
  constructor(
    private notesService: NotesService
  ) { }

  ngOnInit() {
  }

  onBan() {
    this.processing = true;
    this.notesService.ban(this.reportDetails.id)
      .takeWhile(alive => this.alive)
      .subscribe(res => {
        this.onAlterDataEvent.emit(true);
        this.processing = false;
        this.toggleModal();
      }, error => {
        this.toggleModal();
        this.processing = false;
      });
  }

  private toggleModal() {
    try {
      $('#report-note-detail-modal').modal('toggle');
    } catch (exception) {
      console.log(exception);
    }
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
