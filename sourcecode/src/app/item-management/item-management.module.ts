import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from './../shared/shared.module';
import { ItemManagementRoutingModule } from './item-management-routing.module';
import { NotesComponent } from './notes/notes.component';
import { SeriesComponent } from './series/series.component';
import { AddNoteComponent } from './notes/add-note/add-note.component';
import { NoteDescriptionComponent } from './notes/note-description/note-description.component';
import { NoteUploadComponent } from './notes/note-upload/note-upload.component';
import { NoteAudioComponent } from './notes/note-audio/note-audio.component';
import { UpdateNoteComponent } from './notes/update-note/update-note.component';
import { NoteImageComponent } from './notes/note-image/note-image.component';
import { AddSeriesComponent } from './series/add-series/add-series.component';
import { SeriesDescriptionComponent } from './series/series-description/series-description.component';
import { AddNoteSeriesComponent } from './series/add-note-series/add-note-series.component';
import { UpdateSeriesComponent } from './series/update-series/update-series.component';
import { CategoriesComponent } from './categories/categories.component';
import { SubjectComponent } from './categories/subject/subject.component';
import { GradeComponent } from './categories/grade/grade.component';
import { DifficultyComponent } from './categories/difficulty/difficulty.component';
import { AddSubjectComponent } from './categories/subject/add-subject/add-subject.component';
import { UpdateSubjectComponent } from './categories/subject/update-subject/update-subject.component';
import { DynamicDirective } from './directive/dynamic.directive';
import { AddGradeComponent } from './categories/grade/add-grade/add-grade.component';
import { UpdateGradeComponent } from './categories/grade/update-grade/update-grade.component';
import { AddDifficultyComponent } from './categories/difficulty/add-difficulty/add-difficulty.component';
import { UpdateDifficultyComponent } from './categories/difficulty/update-difficulty/update-difficulty.component';
import { NoteApprovementComponent } from './note-approvement/note-approvement.component';
import { AprroveNoteComponent } from './note-approvement/aprrove-note/aprrove-note.component';
import { NoteReportComponent } from './note-report/note-report.component';
import { NoteReportDetailComponent } from './note-report/note-report-detail/note-report-detail.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ItemManagementRoutingModule
  ],
  declarations: [
    NotesComponent,
    SeriesComponent,
    AddNoteComponent,
    NoteDescriptionComponent,
    NoteUploadComponent,
    NoteAudioComponent,
    UpdateNoteComponent,
    NoteImageComponent,
    AddSeriesComponent,
    SeriesDescriptionComponent,
    AddNoteSeriesComponent,
    UpdateSeriesComponent,
    CategoriesComponent,
    SubjectComponent,
    GradeComponent,
    DifficultyComponent,
    AddSubjectComponent,
    UpdateSubjectComponent,
    DynamicDirective,
    AddGradeComponent,
    UpdateGradeComponent,
    AddDifficultyComponent,
    UpdateDifficultyComponent,
    NoteApprovementComponent,
    AprroveNoteComponent,
    NoteReportComponent,
    NoteReportDetailComponent
  ],
  entryComponents: [
    SubjectComponent,
    GradeComponent,
    DifficultyComponent
  ],
  exports: [
    AprroveNoteComponent,
    NoteReportDetailComponent
  ]
})
export class ItemManagementModule { }
