import { Component, OnInit, OnDestroy } from '@angular/core';

import { ReportsService } from './../../api/api-services/reports/reports.service';

@Component({
  selector: 'app-note-overview',
  templateUrl: './note-overview.component.html',
  styleUrls: ['./note-overview.component.css']
})
export class NoteOverviewComponent implements OnInit, OnDestroy {
  processing = false;
  // Graph options
  view: any[] = [500, 300];
  multi = [];
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = '';
  showYAxisLabel = true;
  yAxisLabel = '#Notes';
  colorScheme = {
    domain: ['#5AA454', '#A10A28']
  };
  autoScale = true;
  private alive = true;
  private queryBy = 'day';
  constructor(
    private reportsService: ReportsService
  ) { }

  ngOnInit() {
    this.getNotesOverview();
  }

  onQueryByClick(queryBy) {
    this.queryBy = queryBy;
    this.getNotesOverview();
  }

  private getNotesOverview() {
    this.processing = true;
    this.reportsService.getNotesOverview(null, null, null, 'paid', this.queryBy)
      .takeWhile(alive => this.alive)
      .subscribe(res => {
        this.multi = res;
        this.processing = false;
      }, error => {
        console.log(error);
        this.processing = false;
      });
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
