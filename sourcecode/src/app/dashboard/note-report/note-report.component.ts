import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { ReportService } from './../../api/api-services/item-management/report.service';
import { DashboardService } from './../../api/api-services/dashboard/dashboard.service';

@Component({
  selector: 'app-note-report',
  templateUrl: './note-report.component.html',
  styleUrls: ['./note-report.component.css']
})
export class NoteReportComponent implements OnInit, OnDestroy {

  dataService;
  columnList = [{ prop: 'reportedNumber', name: '#Reported' }, { prop: 'id', name: 'Note ID' },
  { prop: 'name', name: 'Note Name' }, { prop: 'addedDate', name: 'Lasted Date' },
  { prop: 'status', name: 'Status' }, { name: 'Action' }];
  reports;
  onAlterData = { flag: false };
  searchForm;
  _searchForm;

  // Graph options
  multi: any[];
  view: any[] = [1000, 120];
  showXAxis = true;
  showYAxis = true;
  gradient = true;
  showLegend = true;
  showXAxisLabel = false;
  xAxisLabel = 'Country';
  showYAxisLabel = false;
  yAxisLabel = 'Population';
  colorScheme = {
    domain: ['#A10A28', '#5AA454', '#C7B42C', '#AAAAAA']
  };
  private alive = true;
  constructor(
    private reportService: ReportService,
    private dashboardService: DashboardService
  ) {
    this.dataService = this.reportService;
  }

  ngOnInit() {
    setTimeout(() => {
      this.initialFormData();
    }, 1000);
    this.getNoteReportSummary('day');
  }

  onTableRowEdit($event) {
    this.reports = JSON.parse(JSON.stringify($event));
  }

  onAlterDataEvent($event) {
    this.onAlterData = { flag: true };
  }

  onQueryByClick(queryBy) {
    this.searchForm.get('queryBy').setValue(queryBy);
    this.getNoteReportSummary(queryBy);
  }

  private initialFormData() {
    try {
      this.searchForm = new FormBuilder().group({
        queryBy: ['day']
      });
      this._searchForm = { form: this.searchForm };
      this.searchForm.valueChanges.subscribe(data => {
        this._searchForm = { form: this.searchForm };
      });
    } catch (exception) {
      console.log(exception);
    }
  }

  private getNoteReportSummary(queryBy) {
    this.dashboardService.getNotesReportSummary(queryBy)
      .takeWhile(alive => this.alive)
      .subscribe(data => {
        this.multi = data;
      }, error => {
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
