import { Routes, RouterModule } from '@angular/router';
import { NgModule, Component } from '@angular/core';

import { DashboardComponent } from './dashboard/dashboard.component';

const dashboardRoutes: Routes = [
    { path: 'dashboard', component: DashboardComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(dashboardRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class DashboardRoutingModule { }
