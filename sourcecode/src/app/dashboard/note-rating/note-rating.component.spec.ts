import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoteRatingComponent } from './note-rating.component';

describe('NoteRatingComponent', () => {
  let component: NoteRatingComponent;
  let fixture: ComponentFixture<NoteRatingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoteRatingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoteRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
