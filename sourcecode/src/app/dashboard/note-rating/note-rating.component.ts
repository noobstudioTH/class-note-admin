import { Component, OnInit, OnDestroy } from '@angular/core';
import { DashboardService } from './../../api/api-services/dashboard/dashboard.service';

@Component({
  selector: 'app-note-rating',
  templateUrl: './note-rating.component.html',
  styleUrls: ['./note-rating.component.css']
})
export class NoteRatingComponent implements OnInit, OnDestroy {

  processing = false;
  // Graph options
  showXAxis = true;
  showYAxis = true;
  gradient = true;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = 'Rating';
  showYAxisLabel = true;
  yAxisLabel = 'Rating Total';
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  multi;
  view: any[] = [500, 300];
  private alive = true;
  constructor(
    private dashboardService: DashboardService
  ) { }

  ngOnInit() {
    this.getNotesRatingSummary('day');
  }

  onQueryByClick(queryBy) {
    this.getNotesRatingSummary(queryBy);
  }

  private getNotesRatingSummary(queryBy) {
    this.processing = true;
    this.dashboardService.getNotesRatingSummary(queryBy)
      .takeWhile(alive => this.alive)
      .subscribe(data => {
        this.multi = data;
        this.processing = false;
      }, error => {
        this.processing = false;
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
