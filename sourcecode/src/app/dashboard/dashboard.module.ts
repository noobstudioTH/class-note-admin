import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ItemManagementModule } from './../item-management/item-management.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { DashboardComponent } from './dashboard/dashboard.component';
import { NoteApprovementComponent } from './note-approvement/note-approvement.component';
import { NoteOverviewComponent } from './note-overview/note-overview.component';
import { NoteRatingComponent } from './note-rating/note-rating.component';
import { NoteReportComponent } from './note-report/note-report.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ItemManagementModule,
    DashboardRoutingModule,
    NgxChartsModule
  ],
  declarations: [
    DashboardComponent,
    NoteApprovementComponent,
    NoteOverviewComponent,
    NoteRatingComponent,
    NoteReportComponent
  ]
})
export class DashboardModule { }
