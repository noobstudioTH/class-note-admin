import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { NotesService } from './../../api/api-services/item-management/notes.service';
import { StatusService } from './../../api/api-services/master/status.service';
import { DashboardService } from './../../api/api-services/dashboard/dashboard.service';

@Component({
  selector: 'app-note-approvement',
  templateUrl: './note-approvement.component.html',
  styleUrls: ['./note-approvement.component.css']
})
export class NoteApprovementComponent implements OnInit, OnDestroy {

  dataService;
  columnList = [{ prop: 'status', name: 'Status' }, { prop: 'addedDate', name: 'Date Added' },
  { prop: 'id', name: 'Note ID' }, { prop: 'name', name: 'Note Name' },
  { prop: 'writername', name: 'Writer Name' }, { name: 'Action' }];
  searchForm;
  _searchForm;
  selectedNote;
  onAlterData = { flag: false };
  sortingOption;
  processing = false;

  // Graph options
  view: any[] = [1000, 300];
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  multi;
  gradient = true;
  private alive = true;
  constructor(
    private statusService: StatusService,
    private dashboardService: DashboardService
  ) {
    this.dataService = dashboardService;
    this.sortingOption = this.statusService.get();
  }

  ngOnInit() {
    setTimeout(() => {
      this.initialFormData();
    }, 1000);
    this.getNotesStatusSummary('day');
  }

  onTableRowEdit($event) {
    this.selectedNote = { note: $event };
  }

  onAlterDataEvent($event) {
    this.onAlterData = { flag: true };
    this.getNotesStatusSummary(this.searchForm.get('queryBy').value);
  }

  onQueryByClick(queryBy) {
    this.getNotesStatusSummary(queryBy);
    this.searchForm.get('queryBy').setValue(queryBy);
  }

  private initialFormData() {
    try {
      this.searchForm = new FormBuilder().group({
        status: ['pending'],
        queryBy: ['day']
      });
      this._searchForm = { form: this.searchForm };
      this.searchForm.valueChanges.subscribe(data => {
        this._searchForm = { form: this.searchForm };
      });
    } catch (exception) {
      console.log(exception);
    }
  }

  private getNotesStatusSummary(queryBy) {
    this.processing = true;
    this.dashboardService.getNotesStatusSummary(queryBy)
      .takeWhile(alive => this.alive)
      .subscribe(data => {
        this.multi = data;
        this.processing = false;
      }, error => {
        this.processing = false;
        console.log(error);
      });
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
