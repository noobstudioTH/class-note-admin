import { Component, OnInit, OnDestroy } from '@angular/core';

import { ProfileService } from './../../api/api-services/profile.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  private alive = true;
  constructor(private profileService: ProfileService, private router: Router) {}

  ngOnInit() {}

  logout() {
    this.profileService
      .logout()
      .takeWhile(alive => this.alive)
      .subscribe(
        res => {
          window.location.href = environment.logoutPath;
        },
        error => {
          console.log(error);
        }
      );
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
