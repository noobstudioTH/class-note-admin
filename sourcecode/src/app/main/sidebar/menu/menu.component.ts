import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  menuItemList = [
    { name: 'Dashboard', value: 'dashboard', path: '/dashboard', subMenuItemList: null, iconClass: 'fa-tachometer-alt' },
    {
      name: 'Item Management', value: 'itemManagement', path: '', toggle: false, subMenuItemList: [
        { name: 'Notes', value: 'notes', path: '/notes' },
        { name: 'Series', value: 'series', path: '/series' },
        { name: 'Categories', value: 'categories', path: '/categories' },
        { name: 'Note Approvement', value: 'noteApprovement', path: '/note-approvement' },
        { name: 'Reported Notes', value: 'reportedNaote', path: '/note-report' },
      ], iconClass: 'fa-folder-open'
    },
    {
      name: 'User Management', value: 'userManagement', path: '', toggle: false, subMenuItemList: [
        { name: 'Users', value: 'users', path: '/users' }
      ], iconClass: 'fa-user'
    },
    {
      name: 'Report', value: 'reports', path: '', toggle: false, subMenuItemList: [
        { name: 'Transactions', value: 'transactions', path: '/transactions' },
        { name: 'Notes Overview', value: 'notesOverview', path: '/note-overview' },
        { name: 'Real-Time', value: 'realtime', path: '/real-time' },
        { name: 'Visitors', value: 'visitors', path: '/visitors' },
        { name: 'Time-Spent', value: 'timespent', path: '/time-spent' },
        {
          name: 'Ranking', value: 'ranking', path: '', toggle: false, subMenuItemList: [
            { name: 'Notes', value: 'ranking-notes', path: '/ranking-notes' },
            { name: 'User', value: 'ranking-user', path: '/ranking-user' },
            { name: 'Search', value: 'ranking-search', path: '/ranking-search' },
          ]
        }
      ], iconClass: 'fa-chart-bar'
    },
  ];

  constructor() { }

  ngOnInit() {
  }

  onMenuClick(menuItem) {
    menuItem.toggle = !menuItem.toggle;
  }

}
