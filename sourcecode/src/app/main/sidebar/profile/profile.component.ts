import { Component, OnInit, OnDestroy } from '@angular/core';

import 'rxjs/add/operator/takeWhile';

import { ProfileService } from './../../../api/api-services/profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {
  profile;
  private alive = true;

  constructor(private profileService: ProfileService) {}

  ngOnInit() {
    this.getProfile();
  }

  private getProfile() {
    this.profileService.profileSubject.takeWhile(alive => this.alive).subscribe(
      profile => {
        try {
          this.profile = profile.data;
          this.profile.photo = this.profileService.getProfilePhoto();
        } catch (exception) {
          console.log(exception);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
