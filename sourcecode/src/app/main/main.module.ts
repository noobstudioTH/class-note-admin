import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ProfileComponent } from './sidebar/profile/profile.component';
import { MenuComponent } from './sidebar/menu/menu.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    HeaderComponent,
    SidebarComponent,
    ProfileComponent,
    MenuComponent
  ],
  exports: [
    HeaderComponent,
    SidebarComponent
  ]
})
export class MainModule { }
