export enum Notes {
    noteId = 'noteId',
    noteName = 'noteName',
    writerName = 'writerName',
    privacy = 'privacy',
    status = 'status'
}

export enum NoteItem {
    key = 'key',
    caption = 'caption',
    grade = 'grade',
    keywords = 'keywords',
    level = 'level',
    privacy = 'privacy',
    subject = 'subject',
    series_id = 'series_id'
}
