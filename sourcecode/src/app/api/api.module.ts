import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { ApiService } from './api.service';
import { ProfileService } from './api-services/profile.service';
import { NotesService } from './api-services/item-management/notes.service';
import { PrivacyService } from './api-services/master/privacy.service';
import { SeriesService } from './api-services/item-management/series.service';
import { SubjectsService } from './api-services/master/subjects.service';
import { LevelsService } from './api-services/master/levels.service';
import { GradesService } from './api-services/master/grades.service';
import { ReportService } from './api-services/item-management/report.service';
import { StatusService } from './api-services/master/status.service';
import { UsersService } from './api-services/user-management/users.service';
import { ReportsService } from './api-services/reports/reports.service';
import { RankingService } from './api-services/reports/ranking.service';
import { DashboardService } from './api-services/dashboard/dashboard.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  providers: [
    ApiService,
    ProfileService,
    NotesService,
    PrivacyService,
    SeriesService,
    GradesService,
    LevelsService,
    SubjectsService,
    ReportService,
    StatusService,
    UsersService,
    ReportsService,
    RankingService,
    DashboardService
  ],
  declarations: []
})
export class ApiModule { }
