import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class ApiService {

  constructor(
    private http: HttpClient,
  ) { }

  get(url: string, httpParams?): Observable<any> {
    return this.http.get(url, {
      params: httpParams,
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
      withCredentials: true
    });
  }

  post(url: string, body: any): Observable<any> {
    return this.http
      .post(url, JSON.stringify(body), {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
        withCredentials: true
      });
  }

  delete(url: string, httpParams?): Observable<any> {
    return this.http.delete(url, {
      params: httpParams,
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
      withCredentials: true
    });
  }

  put(url: string, body: any): Observable<any> {
    return this.http
      .put(url, JSON.stringify(body), {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
        withCredentials: true
      });
  }

  getBlob(url): Observable<Blob> {
    return this.http
      .get(url, {
        responseType: 'blob'
      });
  }

}
