import { Injectable } from '@angular/core';

import { environment } from './../../../../environments/environment';
import { ApiService } from './../../api.service';

@Injectable()
export class ReportsService {

  constructor(
    private apiService: ApiService
  ) { }

  getNotesOverview(subject, grade, difficulty, privacy, queryBy) {
    const url = `${environment.appApiServer}admin/notes/overview`;
    const body = {
      subject: subject,
      grade: grade,
      difficulty: difficulty,
      privacy: privacy,
      queryBy: queryBy
    };
    return this.apiService.post(url, body);
  }

}
