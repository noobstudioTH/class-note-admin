import { Injectable } from '@angular/core';

import { environment } from './../../../../environments/environment';
import { ApiService } from './../../api.service';

@Injectable()
export class RankingService {

  constructor(
    private apiService: ApiService
  ) { }

  getRankingNotes(options) {
    const url = `${environment.appApiServer}admin/ranking/notes`;
    const body = {
      start: options ? options.start : null,
      end: options ? options.end : null,
      sortBy: options ? options.sortingBy : null,
      queryBy: options ? options.queryBy : null
    };
    return this.apiService.post(url, body);
  }

  getRankingSearch(options) {
    const url = `${environment.appApiServer}admin/ranking/search`;
    const body = {
      start: options ? options.start : null,
      end: options ? options.end : null,
      sortBy: options ? options.sortingBy : null,
      queryBy: options ? options.queryBy : null
    };
    return this.apiService.post(url, body);
  }
}
