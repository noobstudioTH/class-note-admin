import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { environment } from './../../../../environments/environment';
import { ApiService } from './../../api.service';

@Injectable()
export class DashboardService {

  constructor(
    private apiService: ApiService
  ) { }

  getData(searchCongig, searchForm?: FormGroup) {
    const url = `${environment.appApiServer}admin/dashboard/note-status-all`;
    const body = {
      start: searchCongig ? searchCongig.start : null,
      end: searchCongig ? searchCongig.end : null,
      sortBy: searchCongig ? searchCongig.sortBy : null,
      sortType: searchCongig ? searchCongig.sortType : null
    };
    this.buildGetDataBody(body, searchForm);
    return this.apiService.post(url, body);
  }

  getNotesStatusSummary(queryBy) {
    const url = `${environment.appApiServer}admin/dashboard/note-status-summary`;
    const body = {
      queryBy: queryBy
    };
    return this.apiService.post(url, body);
  }

  getNotesRatingSummary(queryBy) {
    const url = `${environment.appApiServer}admin/dashboard/note-rating-summary`;
    const body = {
      queryBy: queryBy
    };
    return this.apiService.post(url, body);
  }

  getNotesReportSummary(queryBy) {
    const url = `${environment.appApiServer}admin/dashboard/note-report-summary`;
    const body = {
      queryBy: queryBy
    };
    return this.apiService.post(url, body);
  }

  private buildGetDataBody(body, searchForm: FormGroup) {
    try {
      if (searchForm) {
        body.search = {
          status: searchForm.get('status') ? searchForm.get('status').value : null,
          queryBy: searchForm.get('queryBy') ? searchForm.get('queryBy').value : null
        };
      }
    } catch (exception) {
      console.log(exception);
    }
  }
}
