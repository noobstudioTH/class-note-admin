import { Injectable } from '@angular/core';

import { environment } from './../../../../environments/environment';
import { ApiService } from './../../api.service';
import { FormGroup } from '@angular/forms';

@Injectable()
export class UsersService {

  constructor(
    private apiService: ApiService
  ) { }

  getData(searchCongig, searchForm?: FormGroup) {
    const url = `${environment.appApiServer}admin/users`;
    const body = {
      start: searchCongig ? searchCongig.start : null,
      end: searchCongig ? searchCongig.end : null,
      sortBy: searchCongig ? searchCongig.sortBy : null,
      sortType: searchCongig ? searchCongig.sortType : null
    };
    this.buildGetDataBody(body, searchForm);
    return this.apiService.post(url, body);
  }

  private buildGetDataBody(body, searchForm: FormGroup) {
    try {
      if (searchForm) {
        body.search = {

        };
      }
    } catch (exception) {
      console.log(exception);
    }
  }
}
