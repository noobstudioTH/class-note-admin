import { Injectable } from '@angular/core';

import { ReplaySubject } from 'rxjs/ReplaySubject';

import { environment } from './../../../environments/environment';
import { ApiService } from './../api.service';

@Injectable()
export class ProfileService {
  profileSubject = new ReplaySubject<any>(1);
  constructor(private apiService: ApiService) {}

  getProfileAdmin(uid) {
    const url = `${environment.appApiServer}login`;
    return this.apiService.post(url, { uid: uid });
  }

  getProfilePhoto() {
    return `${environment.appResourceServer}default/user.png`;
  }

  logout() {
    const url = `${environment.appApiServer}logout`;
    return this.apiService.post(url, {});
  }
}
