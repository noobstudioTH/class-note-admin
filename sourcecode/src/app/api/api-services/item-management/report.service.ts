import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { environment } from './../../../../environments/environment';
import { Notes, NoteItem } from './../../../enum/notes.enum';
import { ApiService } from './../../api.service';
@Injectable()
export class ReportService {

  constructor(
    private apiService: ApiService
  ) { }

  getData(searchCongig, searchForm?: FormGroup) {
    const url = `${environment.appApiServer}admin/notes/report`;
    const body = {
      start: searchCongig.start,
      end: searchCongig.end,
      sortBy: searchCongig.sortBy,
      sortType: searchCongig.sortType
    };
    this.buildGetDataBody(body, searchForm);
    return this.apiService.post(url, body);
  }

  private buildGetDataBody(body, searchForm: FormGroup) {
    try {
      if (searchForm) {
        body.search = {
          key: searchForm.get(Notes.noteId) ? searchForm.get(Notes.noteId).value : null,
          caption: searchForm.get(Notes.noteName) ? searchForm.get(Notes.noteName).value : null,
          privacy: searchForm.get(Notes.privacy) ? searchForm.get(Notes.privacy).value : null,
          status: searchForm.get(Notes.status) ? searchForm.get(Notes.status).value : null,
          writerName: searchForm.get(Notes.writerName) ? searchForm.get(Notes.writerName).value : null,
          queryBy: searchForm.get('queryBy') ? searchForm.get('queryBy').value : null
        };
      }
    } catch (exception) {
      console.log(exception);
    }
  }
}
