import { FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';

import { environment } from './../../../../environments/environment';
import { Series } from './../../../enum/series.enum';
import { ApiService } from './../../api.service';

@Injectable()
export class SeriesService {

  constructor(
    private apiService: ApiService
  ) { }

  getSeriesMaster() {
    const url = `${environment.appApiServer}admin/series-master`;
    return this.apiService.get(url);
  }

  getData(searchCongig, searchForm?: FormGroup) {
    const url = `${environment.appApiServer}admin/series`;
    const body = {
      start: searchCongig.start,
      end: searchCongig.end,
      sortBy: searchCongig.sortBy,
      sortType: searchCongig.sortType
    };
    this.buildGetDataBody(body, searchForm);
    return this.apiService.post(url, body);
  }

  create(name, description, notes) {
    const url = `${environment.appApiServer}series`;
    const body = {
      name: name,
      description: description,
      notes: notes
    };
    return this.apiService.post(url, body);
  }

  update(id, name, description, notes) {
    const url = `${environment.appApiServer}series/${id}`;
    const body = {
      name: name,
      description: description,
      notes: notes
    };
    return this.apiService.put(url, body);
  }

  delete(id) {
    const url = `${environment.appApiServer}series/${id}`;
    return this.apiService.delete(url);
  }

  private buildGetDataBody(body, searchForm: FormGroup) {
    if (searchForm) {
      body.search = {
        key: searchForm.get(Series.seriesId).value,
        caption: searchForm.get(Series.seriesName).value,
        writerName: searchForm.get(Series.writerName).value
      };
    }
  }

}
