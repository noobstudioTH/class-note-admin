import { FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';

import { environment } from './../../../../environments/environment';
import { Notes, NoteItem } from './../../../enum/notes.enum';
import { ApiService } from './../../api.service';

@Injectable()
export class NotesService {

  constructor(
    private apiService: ApiService
  ) { }

  getAll() {
    const url = `${environment.appApiServer}admin/notes`;
    return this.apiService.get(url);
  }

  getNotes(params) {
    const url = `${environment.appApiServer}notes`;
    return this.apiService.get(url, params);
  }

  getData(searchCongig, searchForm?: FormGroup) {
    const url = `${environment.appApiServer}admin/notes`;
    const body = {
      start: searchCongig ? searchCongig.start : null,
      end: searchCongig ? searchCongig.end : null,
      sortBy: searchCongig ? searchCongig.sortBy : null,
      sortType: searchCongig ? searchCongig.sortType : null
    };
    this.buildGetDataBody(body, searchForm);
    return this.apiService.post(url, body);
  }

  create(createForm) {
    const url = `${environment.appApiServer}notes`;
    return this.apiService.post(url, this.buildNoteForm(createForm));
  }

  delete(key) {
    const url = `${environment.appApiServer}notes/${key}`;
    return this.apiService.delete(url);
  }

  update(createForm) {
    const url = `${environment.appApiServer}notes/${createForm.get(NoteItem.key).value}`;
    return this.apiService.put(url, this.buildNoteForm(createForm));
  }

  deleteFile(key, mediaId) {
    const url = `${environment.appApiServer}notes/${key}/media/${mediaId}`;
    return this.apiService.delete(url);
  }

  deleteAudio(key, mediaId) {
    const url = `${environment.appApiServer}notes/${key}/media/${mediaId}/audio`;
    return this.apiService.delete(url);
  }

  approve(id) {
    const url = `${environment.appApiServer}admin/notes/approve/${id}`;
    return this.apiService.get(url);
  }

  reject(id) {
    const url = `${environment.appApiServer}admin/notes/reject/${id}`;
    return this.apiService.get(url);
  }

  ban(id) {
    const url = `${environment.appApiServer}admin/notes/ban/${id}`;
    return this.apiService.get(url);
  }

  getReviews(id) {
    const url = `${environment.appApiServer}/notes/${id}/reviews`;
    return this.apiService.get(url);
  }

  private buildGetDataBody(body, searchForm: FormGroup) {
    try {
      if (searchForm) {
        body.search = {
          key: searchForm.get(Notes.noteId) ? searchForm.get(Notes.noteId).value : null,
          caption: searchForm.get(Notes.noteName) ? searchForm.get(Notes.noteName).value : null,
          privacy: searchForm.get(Notes.privacy) ? searchForm.get(Notes.privacy).value : null,
          status: searchForm.get(Notes.status) ? searchForm.get(Notes.status).value : null,
          writerName: searchForm.get(Notes.writerName) ? searchForm.get(Notes.writerName).value : null
        };
      }
    } catch (exception) {
      console.log(exception);
    }
  }

  private buildNoteForm(createForm) {
    let body = null;
    try {
      body = {
        caption: createForm.get(NoteItem.caption).value,
        grade: createForm.get(NoteItem.grade).value,
        keywords: this.transformKeyword(createForm.get(NoteItem.keywords).value),
        level: createForm.get(NoteItem.level).value,
        privacy: createForm.get(NoteItem.privacy).value,
        subject: createForm.get(NoteItem.subject).value,
        series_id: createForm.get(NoteItem.series_id) ? createForm.get(NoteItem.series_id).value : null
      };
    } catch (exception) {
      console.log(exception);
    } finally {
      return body;
    }
  }

  private transformKeyword(keywordsIn) {
    const keywords = [];
    try {
      keywordsIn.forEach(keyword => {
        keywords.push(keyword.value);
      });
    } catch (exception) {
      console.log(exception);
    } finally {
      return keywords;
    }
  }
}
