import { Injectable } from '@angular/core';

import { environment } from './../../../../environments/environment';
import { ApiService } from './../../api.service';

@Injectable()
export class StatusService {

  constructor(
    private apiService: ApiService
  ) { }

  get() {
    const url = `${environment.appApiServer}status`;
    return this.apiService.get(url);
  }

}
