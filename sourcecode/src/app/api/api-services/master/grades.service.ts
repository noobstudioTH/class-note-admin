import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { environment } from './../../../../environments/environment';
import { ApiService } from './../../api.service';

@Injectable()
export class GradesService {

  constructor(
    private apiService: ApiService
  ) { }

  get() {
    const url = `${environment.appApiServer}grades`;
    return this.apiService.get(url);
  }

  getData(searchCongig, searchForm?: FormGroup) {
    const url = `${environment.appApiServer}admin/master/data/grade`;
    const body = {
      start: searchCongig.start,
      end: searchCongig.end,
      sortBy: searchCongig.sortBy,
      sortType: searchCongig.sortType
    };
    return this.apiService.post(url, body);
  }

  create(code, name) {
    const url = `${environment.appApiServer}admin/master/grade`;
    const body = {
      code: code,
      text: name
    };
    return this.apiService.post(url, body);
  }

  update(code, name) {
    const url = `${environment.appApiServer}admin/master/grade`;
    const body = {
      code: code,
      text: name
    };
    return this.apiService.put(url, body);
  }

  delete(dataList) {
    const url = `${environment.appApiServer}admin/master/delete/grade`;
    const body = dataList;
    return this.apiService.post(url, body);
  }

}
