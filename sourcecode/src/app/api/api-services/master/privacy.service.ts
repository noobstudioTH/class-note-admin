import { Injectable } from '@angular/core';

import { environment } from './../../../../environments/environment';
import { ApiService } from './../../api.service';

@Injectable()
export class PrivacyService {

  constructor(
    private apiService: ApiService
  ) { }

  get() {
    const url = `${environment.appApiServer}privacy`;
    return this.apiService.get(url);
  }

}
