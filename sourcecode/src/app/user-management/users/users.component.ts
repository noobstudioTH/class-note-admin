import { Component, OnInit, OnDestroy } from '@angular/core';

import { UsersService } from './../../api/api-services/user-management/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, OnDestroy {

  dataService;
  columnList = [{ prop: 'id', name: 'User ID' }, { prop: 'name', name: 'User Name' },
  { prop: 'writername', name: 'Date Added' }, { prop: 'privacy', name: 'Status' }, { name: 'Action' }];
  formConfig = [
    [{ id: '', name: 'User Id', type: 'text', placeholder: 'User Id' },
    { id: '', name: 'User Name', type: 'text', placeholder: 'User Name' }],
    [{ id: '', name: 'Date Added', type: 'text', placeholder: 'Date Added' },
    { id: '', name: 'Status', type: 'text', placeholder: 'Status' }]
  ];
  searchForm;
  addUser;
  selectedNote;
  onAlterData = { flag: false };
  private alive = true;
  constructor(
    private usersService: UsersService
  ) {
    this.dataService = this.usersService;
  }

  ngOnInit() {
  }

  onsubmitEvent($event) {
    try {
      this.searchForm = { form: $event };
    } catch (exception) {
      console.log(exception);
    }
  }

  onAddUser() {
    this.addUser = {};
  }

  onTableRowEdit($event) {
    this.selectedNote = { note: JSON.parse(JSON.stringify($event)) };
  }

  onAlterDataEvent($event) {
    this.onAlterData = { flag: true };
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
