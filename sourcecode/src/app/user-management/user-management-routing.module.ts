import { Routes, RouterModule } from '@angular/router';
import { NgModule, Component } from '@angular/core';

import { UsersComponent } from './users/users.component';

const userManagementRoutes: Routes = [
    { path: 'users', component: UsersComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(userManagementRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class UserManagementRoutingModule { }
