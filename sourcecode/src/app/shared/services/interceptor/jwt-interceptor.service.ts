import { finalize } from 'rxjs/operators/finalize';
import { tap } from 'rxjs/operators/tap';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class JwtInterceptorService implements HttpInterceptor {

  constructor() { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap(error => {
        if (error instanceof HttpErrorResponse) {
          if (error.status === 405) {
            console.log(error.status);
          }
        }
      })
    );
  }
}

