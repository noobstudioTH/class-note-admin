import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxSpinnerComponent } from './box-spinner.component';

describe('BoxSpinnerComponent', () => {
  let component: BoxSpinnerComponent;
  let fixture: ComponentFixture<BoxSpinnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxSpinnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxSpinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
