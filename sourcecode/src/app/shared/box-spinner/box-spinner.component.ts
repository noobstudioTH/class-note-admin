import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-box-spinner',
  templateUrl: './box-spinner.component.html',
  styleUrls: ['./box-spinner.component.css']
})
export class BoxSpinnerComponent implements OnInit {

  loading;
  @Input()
  set processing(processing) {
    this.loading = processing;
  }
  constructor() { }

  ngOnInit() {
  }

}
