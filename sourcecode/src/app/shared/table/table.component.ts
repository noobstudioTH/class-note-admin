import { Component, OnInit, OnDestroy, Input, ViewEncapsulation, ViewChild, TemplateRef, EventEmitter, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TableComponent implements OnInit, OnDestroy {

  _columnsList;
  _dataService;
  _searchForm;
  dataList;
  toggleTable;
  @Input()
  set dataService(dataService) {
    this._dataService = dataService;
  }
  @Input() editable;
  @Input() viewable;
  @ViewChild('editTmpl') editTmpl: TemplateRef<any>;
  @ViewChild('hdrTpl') hdrTpl: TemplateRef<any>;
  @Input()
  set columnsList(columnsList) {
    this._columnsList = columnsList;
    if (this._columnsList) {
      this._columnsList.forEach(column => {
        if (column.name === 'Action') {
          column.cellTemplate = this.editTmpl;
          column.headerTemplate = this.hdrTpl;
        }
      });
      this.initialDataList();
      this.getDataList();
    }
  }
  @Input()
  set searchForm(searchForm) {
    if (searchForm) {
      this._searchForm = searchForm.form;
      this.dataList.rows = [];
      this.dataList.start = 0;
      this.dataList.end = 10;
      this.dataList.offset = 0;
      this.getDataList();
    }
  }
  @Input()
  set onAlterData(alterData) {
    if (alterData.flag) {
      this.getDataList();
    }
  }
  @Input() selectable;
  @Output() onTableRowEdit = new EventEmitter();
  @Output() onTableRowSelect = new EventEmitter();
  selected = [];
  private alive = true;

  constructor() { }

  ngOnInit() {
  }

  setPage($event) {
    this.dataList.start = $event.offset * $event.pageSize;
    this.dataList.end = this.dataList.start + $event.pageSize;
    this.dataList.offset = $event.offset;
    this.getDataList();
  }

  onSort($event) {
    try {
      const sort = $event.sorts[0];
      this.dataList.sortBy = sort.prop;
      this.dataList.sortType = sort.dir;
      this.dataList.start = 0;
      this.dataList.end = 10;
      this.getDataList();
    } catch (exception) {
      console.log(exception);
    }
  }

  onEdit($event) {
    this.onTableRowEdit.emit($event);
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    this.onTableRowSelect.emit(this.selected);
  }

  private initialDataList() {
    this.dataList = {
      offset: 0,
      limit: 10,
      loadingIndicator: false,
      columns: this._columnsList,
      rows: [],
      total: 0,
      start: 0,
      end: 10
    };
  }

  private getDataList() {
    this.dataList.loadingIndicator = true;
    this._dataService.getData(this.dataList, this._searchForm)
      .takeWhile(alive => this.alive)
      .subscribe(dataList => {
        try {
          this.dataList.rows = dataList.data;
          this.dataList.total = dataList.total;
        } catch (exception) {
          console.log(exception);
        } finally {
          this.dataList.loadingIndicator = false;
        }
      }, error => {
        console.log(error);
        this.dataList.loadingIndicator = false;
      });
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
