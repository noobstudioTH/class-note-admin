import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-form-search',
  templateUrl: './form-search.component.html',
  styleUrls: ['./form-search.component.css']
})
export class FormSearchComponent implements OnInit {

  _formConfig;
  searchForm: FormGroup;

  @Input()
  set formConfig(formConfig) {
    if (formConfig) {
      this._formConfig = formConfig;
      this.createForm();
    }
  }
  @Output() createFormEvent = new EventEmitter();
  @Output() onsubmitEvent = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  onSubmit() {
    this.onsubmitEvent.emit(this.searchForm);
  }

  private createForm() {
    this.searchForm = new FormBuilder().group({});
    this._formConfig.forEach(formList => {
      formList.forEach(form => {
        this.searchForm.setControl(form.id, new FormControl());
      });
    });
    this.createFormEvent.emit(this.searchForm);
  }

}
