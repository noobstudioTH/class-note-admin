import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {

  _rating;
  @Input()
  set rating(rating) {
    this._rating = rating;
  }
  constructor() { }

  ngOnInit() {
  }

}
