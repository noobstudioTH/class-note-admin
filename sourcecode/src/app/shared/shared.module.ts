import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { TagInputModule } from 'ngx-chips';
import { FileUploadModule } from 'ng2-file-upload';

import { TableComponent } from './table/table.component';
import { FormSearchComponent } from './form-search/form-search.component';
import { RatingComponent } from './rating/rating.component';
import { BoxSpinnerComponent } from './box-spinner/box-spinner.component';
import { CommingSoonComponent } from './comming-soon/comming-soon.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule
  ],
  declarations: [
    TableComponent,
    FormSearchComponent,
    RatingComponent,
    BoxSpinnerComponent,
    CommingSoonComponent
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    TableComponent,
    FormSearchComponent,
    TagInputModule,
    FileUploadModule,
    RatingComponent,
    BoxSpinnerComponent
  ]
})
export class SharedModule { }
